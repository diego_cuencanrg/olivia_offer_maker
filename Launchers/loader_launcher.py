from EsiosWebService import esios_web_functions as web_service
from managers.mysql_manager import MySQL_Manager
import constants as cons
import datetime
from datetime import timedelta

inicial_raw_date = "2018-12-01"
final_raw_date = "2019-12-01"

start_date = datetime.datetime.strptime(inicial_raw_date, "%Y-%m-%d")
end_date = datetime.datetime.strptime(final_raw_date, "%Y-%m-%d")
list_ids = ["793","794","796","797","798","799","800","1285","1276","802","803","600","764","763"]


dict_values = cons.dict_values_DB

mysql = MySQL_Manager(dict_connection_values=dict_values)
# web_service.fixing_esios(mysql =mysql, start_date = start_date, end_date= end_date, list_ids= list_ids, on_update=True)

second_date = start_date
while (second_date != end_date):
	second_date = second_date +  timedelta(days=1)
	web_service.fixing_kest(mysql=mysql, start_date = second_date, download_folder="C:/Users/Diego/Documents/PWR_Paramas_AB/k_files", on_update=True)
