# Database constants
USER = "NRGLan"
PASSWORD = "L4nMS0"
HOST = "192.168.1.2"

USER_LOCAL = "diego"
PASSWORD_LOCAL = "diego"
HOST_LOCAL = "127.0.0.1"

PORT = "3306"
DATABASE = "power_retailer"

TASA_MUNICIPAL = 1.01522843

dict_values_DB = {
	"user" : USER,
	"password": PASSWORD,
	"host": HOST,
	"database": DATABASE,
	"port": PORT
}

# dict_values_DB = {
# 	"user": USER_LOCAL,
# 	"password": PASSWORD_LOCAL,
# 	"host": HOST_LOCAL,
# 	"database": DATABASE,
# 	"port": PORT
# }

# Uploader constants
DAYS_RANGE = 366
DOWNLOAD_FOLDER = "C:/Users/Diego/Documents/PWR_Params_AB/k_files"
REPORT_FOLDER = "C:/Users/Diego/Documents/Reportes Electricidad/"

# ESIOS PARAMS
ESIOS_PARAMS = ["793","794","796","797","798","799","800","1285","1276","802","803","600","764","763","1366"]

ESIOS_DICT = {
	"600": "Precio OMIE",
	"764": "Precio de los desvíos medidos a subir",
	"793": "Precio medio horario componente restricciones PBF contratación libre",
	"794": "Precio medio horario componente restricciones tiempo real contratación libre",
	"796": "Precio medio horario componente restricciones intradiario contratación libre",
	"797": "Precio medio horario componente reserva de potencia adicional a subir contratación libre",
	"798": "Precio medio horario componente banda secundaria contratación libre",
	"799": "Precio medio horario componente desvios medidos contratación libre",
	"800": "Precio medio horario componente saldo de desvíos contratación libre",
	"802": "Precio medio horario componente saldo P.O.14.6 contratación libre",
	"803": "Precio medio horario componente fallo nominación UPG contratación libre",
	"1285": "Precio medio horario componente control factor potencia contratación libre",
	"1276": "Precio medio horario componente servicio interrumpibilidad contratación libre",
	"763": "Precio de los desvíos medidos a subir"
}

# Tarifas dict
tarifa20A = {
	"pagos_capacidad" :"PC 2.0A",
	"perdidaK": "perd*Kest 2.0A"
}

tarifa21A = {
	"pagos_capacidad" :"PC 2.1A",
	"perdidaK": "perd*Kest 2.1A"
}

tarifa20DHA = {
	"pagos_capacidad" :"PC 2.0DHA",
	"perdidaK": "perd*Kest 2.0DHA"
}

tarifa21DHA = {
	"pagos_capacidad" :"PC 2.1DHA",
	"perdidaK": "perd*Kest 2.1DHA"
}

tarifa20DHS = {
	"pagos_capacidad" :"PC 2.0DHS",
	"perdidaK": "perd*Kest 2.0DHS"
}

tarifa21DHS = {
	"pagos_capacidad" :"PC 2.1DHS",
	"perdidaK": "perd*Kest 2.1DHS"
}



tarifa3A = {
	"pagos_capacidad" : "PC 3.0A",
	"perdidaK" : "per*Kest 3.0A"
}

tarifa31A = {
	"pagos_capacidad": "PC 3.1A",
	"perdidaK": "perd*Kest 3.1A"
}

tarifa61A = {
	"pagos_capacidad": "PC 6",
	"perdidaK": "perd*Kest 6.1A"
}

tarifa61B = {
	"pagos_capacidad": "PC 6",
	"perdidaK": "perd*Kest 6.1B"
}

tarifa62 = {
	"pagos_capacidad": "PC 6",
	"perdidaK": "perd*Kest 6.2"
}

tarifa63 = {
	"pagos_capacidad": "PC 6",
	"perdidaK": "perd*Kest 6.3"
}

tarifa64 = {
	"pagos_capacidad": "PC 3.0A",
	"perdidaK": "perd*Kest 6.4"
}

# Nuevas Tarifas
tarifa20T = {
	"pagos_capacidad": "PC 2.0TD",
	"perdidaK": "perd*Kest 2.0TD"
}

tarifa30T = {
	"pagos_capacidad": "PC 3.0TD",
	"perdidaK": "perd*Kest 3.0TD"
}

tarifa61TD = {
	"pagos_capacidad": "PC 6TD",
	"perdidaK": "perd*Kest 6.1TD"
}

tarifa62TD = {
	"pagos_capacidad": "PC 6TD",
	"perdidaK": "perd*Kest 6.2TD"
}

tarifa63TD = {
	"pagos_capacidad": "PC 6TD",
	"perdidaK": "perd*Kest 6.3TD"
}

tarifa64TD = {
	"pagos_capacidad": "PC 6TD",
	"perdidaK": "perd*Kest 6.4TD"
}


data_om = 0
data_os = 0
costes_gestion = 0
costes_comercializacion = 0.7

# tarifas_ids = {
#     "2.0A": "1",
#     "2.1A": "2" ,
#     "2.0DHA": "3",
#     "2.1DHA": "4",
#     "2.0DHS": "5",
#     "2.1DHS": "6",
#     "3.0A": "7",
#     "3.1A": "8",
#     "6.1A": "9",
#     "6.1B": "10",
#     "6.2": "11",
#     "6.3": "12",
#     "6.4": "13",
#     "2.0TD": "14",
#     "3.0TD": "15",
#     "6.1TD": "16",
#     "6.2TD": "17",
#     "6.3TD": "18",
#     "6.4TD": "19"
# }

tarifas_ids = {
    "2.0TD": "14",
    "3.0TD": "15",
    "6.1TD": "16",
    "6.2TD": "17",
    "6.3TD": "18",
    "6.4TD": "19"
}


pool_year_before = 2018
list_tarifas = ["2.0A","2.1A","2.0DHA","2.1DHA","2.0DHS","2.1DHS","3.0A", "3.1A", "6.1A", "6.1B", "6.2", "6.3", "6.4", "3.0TD", "6.1TD", "6.2TD", "6.3TD", "6.4TD"]

dict_periodos = {
	"2.0A": 1,
    "2.1A": 1 ,
    "2.0DHA": 2,
    "2.1DHA": 2,
    "2.0DHS": 3,
    "2.1DHS": 3,
    "3.0A": 3,
    "3.1A": 3,
    "6.1A": 6,
    "6.1B": 6,
    "6.2": 6,
    "6.3": 6,
    "6.4": 6,
    "2.0TD": 3,
    "3.0TD": 6,
    "6.1TD": 6,
    "6.2TD": 6,
    "6.3TD": 6,
    "6.4TD": 6
}