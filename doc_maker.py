# Older implementation of doc_maker, the actual version is in the Power Retailer Project
# Se mantiene en este proyecto para realización de pruebas


from docx import Document

document = Document('Oferta.IndexadoTramos.docx')

dict = {
	"CLIENTE": "Juan Lopez Jurado",
	"PUNTO": "Calle Albarado",
	"CUPS": "ES1001",
	"TARIFA": "3",
	"CON_P1": 23,
	"CON_P2": 23,
	"CON_P3": 24,
	"PO_P1": 27,
	"PO_P2": 23,
	"PO_P3": 78,
	"FECHA_INICIO": "23/05",
	"FECHA_FIN": "23/06",
	"VIGENCIA": 23,
	"A_P1": 23,
	"A_P2": 23,
	"A_P3": 23,
	"B_P1": 23,
	"B_P2": 23,
	"B_P3": 23
}

for paragraph in document.paragraphs:
	paragraph.text = paragraph.text.format_map(dict)

for table in document.tables:
	for cell in table._cells:
		for paragraph in cell.paragraphs:
			paragraph.text = paragraph.text.format_map(dict)

for section in document.sections:
	for paragraph in section.header.paragraphs:
		paragraph.text = paragraph.text.format_map(dict)

for section in document.sections:
	for paragraph in section.footer.paragraphs:
		paragraph.text = paragraph.text.format_map(dict)

document.save('demo.docx')
