import json
import time
import os

import imaplib
import smtplib
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText

import nrg_sf.common_utils.cripto as cripto
import nrg_sf.common_utils.tools as tools


class Email_Manager:

    def __init__(self,*, email_settings=None, logger=None, decript_key=None):

        self.slug = 'email'

        self.recognized_parameters = ['sender_user',
                                      'sender_password',
                                      'receiver_user',
                                      'receiver_password',
                                      'alerts_inbox']
        if os.path.exists(email_settings):
            if decript_key:
                json_data = cripto.decript_file(file_path=email_settings, cipher_key=decript_key)
                json_email_data = json.loads(json_data)
            else:
                with open(email_settings) as data_file:
                    json_email_data = json.load(data_file)
            for parameter in self.recognized_parameters:
                if parameter in json_email_data['email_settings']:
                    setattr(self, '_' + parameter, json_email_data['email_settings'][parameter])

        self.logger = tools.check_logger(logger)
        logger.debug('Email Initialized')

    def set_sender(self, *, sender_user, sender_password):
        if not sender_password:
            self.logger.debug('sender_password not given')
            return
        if not sender_user:
            self.logger.debug('sender_user not given')
            return
        self._sender_user = sender_user
        self._sender_password = sender_password

    def set_receiver(self, *, receiver_user, receiver_password):
        if not receiver_password:
            self.logger.debug('receiver_password not given')
            return
        if not receiver_user:
            self.logger.debug('receiver_user not given')
            return
        self._receiver_user = receiver_user
        self._receiver_password = receiver_password

    @property
    def sender_parameters(self):
        if not self._sender_password:
            return None
        if not self._sender_user:
            return None
        return {
            'user': self._sender_user,
            'password': self._sender_password,
        }

    @property
    def receiver_parameters(self):
        if not self._receiver_password:
            return None
        if not self._receiver_user:
            return None
        return {
            'user': self._receiver_user,
            'password': self._receiver_password,
        }

    @property
    def alerts_inbox(self):
        if not self._alerts_inbox:
            return None
        return self._alerts_inbox

    def send_email(self, *, email_msg, subject, sender_user=None, sender_password=None, reciver):
        if not sender_user:
            sender_user=self.sender_parameters['user']
        if not sender_password:
            sender_password=self.sender_parameters['password']
        msg = MIMEMultipart('alternative')
        msg['Subject'] = subject
        msg['From'] = sender_user
        msg['To'] = reciver
        msg.attach(MIMEText(email_msg, 'html'))
        mail = smtplib.SMTP('smtp.gmail.com:587')
        mail.ehlo()
        mail.starttls()
        try:
            mail.login(sender_user,
                       sender_password
                       )
        except:
            self.logger.error('Error in email login for user {}'.format(sender_user))
        try:
            mail.sendmail(sender_user,
                          reciver,
                          msg.as_string()
                          )
        except:
            self.logger.error('Error sending email login for user {} to reciver {}'.format(sender_user, reciver))
        mail.quit()

    def send_alert(self, *, alert_msg, alert_subject):
        if not self.sender_parameters:
            self.logger.error('not sender parameters')
            return False
        if not self.alerts_inbox:
            self.logger.error('not alerts inbox parameter')
            return False
        self.send_email(email_msg=alert_msg,
                        subject=alert_subject,
                        sender_user=self.sender_parameters['user'],
                        sender_password=self.sender_parameters['password'],
                        reciver=self.alerts_inbox)
        return True

    def check_email(self, *, mode):
        if mode == 'send':
            if not self.receiver_parameters:
                return False
            if not self.sender_parameters:
                return False
            try:
                mail = smtplib.SMTP('smtp.gmail.com:587')
                mail.ehlo()
                mail.starttls()
                mail.login(self.sender_parameters['user'],
                           self.sender_parameters['password'])
                mail.quit()
                return True
            except:
                return False

        if mode == 'recive':
            if not self.receiver_parameters:
                return False
            try:
                mail = imaplib.IMAP4_SSL('imap.gmail.com')
                mail.login(self.receiver_parameters['user'],
                           self.receiver_parameters['password'])
                mail.close()
                return True
            except:
                return False
