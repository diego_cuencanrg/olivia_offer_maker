from managers.mysql_manager import MySQL_Manager
import constants as cons
import logging.config, logging
from utils.offer_tools import *
from constants import *
from datetime import timedelta, date
from dateutil.relativedelta import *
import calendar
import pandas as pd


logging.config.fileConfig("logging.cfg")
logger = logging.getLogger("OliviaOfertMaker")

mysql = MySQL_Manager(dict_connection_values = cons.dict_values_DB, logger = logger)

def get_PondPool():
	dt_principal = getPool(mysql)
	pool_date_after = date.today() - relativedelta(months=+1)
	pool_date_after_year = pool_date_after.year
	pool_date_after_month = pool_date_after.month

	dt_final = pd.DataFrame(columns=['Year', 'Month', 'Tarifa', '1', '2', '3', '4', '5', '6'])
	dict_periodo = {}
	dict_tarifa = {}

	for x in list_tarifas:
		last_day = calendar.monthrange(pool_date_after_year, pool_date_after_month)[1]
		dt_fecha_0 = dt_principal[dt_principal["edate"] >= date(pool_date_after_year, pool_date_after_month, 1)]
		dt_fecha = dt_fecha_0[dt_fecha_0["edate"] <= date(pool_date_after_year, pool_date_after_month, last_day)]
		for p in (1, 2, 3, 4, 5, 6):
			suma = dt_fecha[dt_fecha[x] == p]["Value"].sum()
			cantidad = len(dt_fecha[dt_fecha[x] == p].index)
			promedio = 0 if cantidad == 0 else (suma / cantidad)
			promedio = 0 if x in ("3.0A", "3.1A") and p > 3 else promedio
			n_3 = str(p)
			dict_periodo[n_3] = promedio
		n_2 = str(x)
		dict_tarifa[n_2] = dict_periodo.copy()
		dt_final = dt_final.append({'Year': pool_date_after_year, 'Month': pool_date_after_month, 'Tarifa': x,
		                            '1': dict_periodo["1"], '2': dict_periodo["2"], '3': dict_periodo["3"],
		                            '4': dict_periodo["4"], '5': dict_periodo["5"], '6': dict_periodo["6"]},
		                           ignore_index=True)
		dict_periodo = {}

	dict_valores = {}
	for index, row in dt_final.iterrows():
		dict_valores["YEAR"] = row["Year"]
		dict_valores["MONTH"]  = row["Month"]
		# dict_valores["TARIFA"] = row["Tarifa"]
		dict_valores["TARIFA"] = tarifas_ids[row["Tarifa"]]
		dict_valores["V1"] = row["1"]
		dict_valores["V2"] = row["2"]
		dict_valores["V3"] = row["3"]
		dict_valores["V4"] = row["4"]
		dict_valores["V5"] = row["5"]
		dict_valores["V6"] = row["6"]
		upload_ppool(mysql,dict_valores)
		print("Insertado en Base de Datos")

def upload_ppool(mysql, dict_valores):
	sql = '''INSERT INTO `power_retailer`.`ponderados_pool`(`Year`,`Month`,`Tarifa`,`1`,`2`,`3`,`4`,`5`,`6`)
	VALUES
	({YEAR},{MONTH},{TARIFA},{V1},{V2},{V3},{V4},{V5},{V6})
	  ON DUPLICATE KEY UPDATE `1` = VALUES(`1`), `2` = VALUES(`2`), `3` =VALUES(`3`), `4` = VALUES(`4`), `5` = VALUES(`5`), `6` = VALUES(`6`);
	'''
	data = list()
	sql = sql.format(YEAR = dict_valores["YEAR"], MONTH = dict_valores["MONTH"], TARIFA = dict_valores["TARIFA"],
	           V1 = dict_valores["V1"], V2 = dict_valores["V2"], V3 = dict_valores["V3"],
	           V4 = dict_valores["V4"], V5 = dict_valores["V5"], V6 = dict_valores["V6"])
	mysql.execute_procedure(query = sql, data = data, to_dataframe=False)


def getAllPPools():
	dt_principal = getPool(mysql)
	pool_date_after = date.today() - relativedelta(months=+1)
	pool_date_after_year = pool_date_after.year
	pool_date_after_month = pool_date_after.month

	dt_final = pd.DataFrame(columns=['Year', 'Month', 'Tarifa', '1', '2', '3', '4', '5', '6'])
	dict_final = {}
	dict_periodo = {}
	dict_tarifa = {}
	for i in range(pool_year_before, pool_date_after_year + 1):
		if i != pool_date_after_year:
			w = 12
		else:
			w = pool_date_after_month
		for z in range(1, w + 1):
			for x in list_tarifas:
				last_day = calendar.monthrange(i, z)[1]
				dt_fecha_0 = dt_principal[dt_principal["edate"] >= date(i, z, 1)]
				dt_fecha = dt_fecha_0[dt_fecha_0["edate"] <= date(i, z, last_day)]
				for p in (1, 2, 3, 4, 5, 6):
					suma = dt_fecha[dt_fecha[x] == p]["Value"].sum()
					cantidad = len(dt_fecha[dt_fecha[x] == p].index)
					promedio = 0 if cantidad == 0 else (suma / cantidad)
					promedio = 0 if x in ("3.0A", "3.1A") and p > 3 else promedio
					n_3 = str(p)
					dict_periodo[n_3] = promedio
				n_2 = str(x)
				dict_tarifa[n_2] = dict_periodo.copy()
				dt_final = dt_final.append({'Year': i, 'Month': z, 'Tarifa': x,
				                            '1': dict_periodo["1"], '2': dict_periodo["2"], '3': dict_periodo["3"],
				                            '4': dict_periodo["4"], '5': dict_periodo["5"], '6': dict_periodo["6"]},
				                           ignore_index=True)
				dict_periodo = {}

			n_1 = str(i) + (("0" + str(z)) if z < 10 else str(z))
			dict_final[n_1] = dict_tarifa.copy()
			dict_tarifa = {}

		dict_valores = {}
		for index, row in dt_final.iterrows():
			dict_valores["YEAR"] = row["Year"]
			dict_valores["MONTH"] = row["Month"]
			# dict_valores["TARIFA"] = row["Tarifa"]
			dict_valores["TARIFA"] = tarifas_ids[row["Tarifa"]]
			dict_valores["V1"] = row["1"]
			dict_valores["V2"] = row["2"]
			dict_valores["V3"] = row["3"]
			dict_valores["V4"] = row["4"]
			dict_valores["V5"] = row["5"]
			dict_valores["V6"] = row["6"]
			upload_ppool(mysql, dict_valores)

