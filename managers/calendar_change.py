from datetime import datetime, timedelta
from collections import OrderedDict

import sys
import calendar

# Detecta si hay un cambio de hora en ese rango de fechas.
# Devuelve:
# Diccionario vacio si no hay cambio de hora
# Diccionario de diccionarios con: sentido (Invierno, Verano) y el día

def detect_calendar_change(start_date, finish_date):
	# Obtenemos los meses que hay entre ambas fechas
	dates = [start_date, finish_date]
	# start, end = [datetime.strptime(x, "%Y-%m-%d") for x in dates]
	start = start_date
	end = finish_date
	list_months = list(OrderedDict(((start + timedelta(x)).strftime(r"%b-%y"), None) for x in range((end - start).days)).keys())
	print("HOLA")
	dict_changes = {}
	# Uno de ellos es marzo u octubre?
	for month in list_months:
		list_changes = []
		if "Mar" in month or "Oct" in month:
			dict_changes[month] = {
				"Year": month.split("-")[1],
				"Month": month.split("-")[0]
			}
			list_changes.append(month)
	# Está incluido el último domingo del mes.
	dict_changes_final = {}
	for key in dict_changes.keys():
		year = int("20" + dict_changes[key]["Year"])
		month = get_month(dict_changes[key]["Month"])
		day = get_last_sunday(year, month)
		sense = "S" if month == "03" else "W"
		date =  str(year) + "-" + month + "-" + str(day)
		dict_changes_final[key] = {
			"Sense": sense,
			"Day": date
		}
	return dict_changes_final

def get_month(month_string):
	list_months_string = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec']
	month_num = list_months_string.index(month_string) + 1
	month = ("0" + str(month_num)) if month_num < 10 else str(month_num)
	return month


def get_last_sunday(year, month):
	return max(week[-1] for week in calendar.monthcalendar(int(year), int(month)))

def change_list(dict_changes, list_info):
	print(dict_changes)
	print(list_info)

	for key in dict_changes.keys():
		if "Mar" in key:
			print("Aplicamos el cambio a la lista")
			fechas_cambiar = filter(filter_date,list_info)
			print(fechas_cambiar)


def filter_date(dates):
	change_date = ""
	if change_date in dates:
		return True
	else:
		return False
