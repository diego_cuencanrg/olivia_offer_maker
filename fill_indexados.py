from EsiosWebService import esios_web_functions as web_service
from managers.mysql_manager import MySQL_Manager
import constants as cons
import datetime
from datetime import timedelta, date
import logging.config,logging
import offer_maker, index_maker
from EsiosWebService.data_checker import *

logging.config.fileConfig("logging.cfg")
logger = logging.getLogger("OliviaOfertMaker")
mysql = MySQL_Manager(dict_connection_values= cons.dict_values_DB, logger = logger)


def get_interval(date_before, date_after):
	diference = date_after - date_before
	days = diference.days
	for single_date in (date_before + timedelta(n) for n in range(days)):
		print(single_date)
		index_maker.main_runner(date_inicial=single_date, mysql=mysql)
		print("Calculado para " + str(single_date))

def get_single_day(date):
	print(date)
	index_maker.main_runner(date_inicial=date, mysql=mysql)
	print("Calculado para " + str(date))



date_before = datetime.datetime.strptime('2021-04-01', '%Y-%m-%d').date() + timedelta(days=1)
date_today = datetime.datetime.strptime('2020-12-01','%Y-%m-%d').date()
date_after = datetime.datetime.strptime("2021-05-01",'%Y-%m-%d').date()
# get_single_day(date_today)
get_interval(date_after = date_after, date_before = date_before)

