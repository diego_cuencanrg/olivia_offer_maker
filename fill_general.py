from EsiosWebService import esios_web_functions as web_service
from managers.mysql_manager import MySQL_Manager
import constants as cons
import datetime
from datetime import timedelta, date
import logging.config,logging
import offer_maker, index_maker
from EsiosWebService.data_checker import *



logging.config.fileConfig("C:/Users/Diego Cuenca/Documents/Olivia OfferOmie/logging.cfg")
logger = logging.getLogger("OliviaOfertMaker")

# Get the actual date
# date_today = date.today() - timedelta(days=2)
date_today = datetime.datetime.strptime('2020-01-01', '%Y-%m-%d').date() + timedelta(days=1)
# print(date_today)
date_before = date_today - timedelta(days=cons.DAYS_RANGE)
date_before = datetime.datetime.strptime('2021-05-01', '%Y-%m-%d').date() + timedelta(days=1)

logger.info("Loading data " + str(date_today))


mysql = MySQL_Manager(dict_connection_values= cons.dict_values_DB, logger = logger)

ESIOS = web_service.fixing_esios(mysql = mysql, start_date = date_before, end_date = date_today,
                         list_ids = cons.ESIOS_PARAMS, on_update=True, logger= logger)
KEST = web_service.fixing_kest(mysql = mysql, start_date = date_today,
                         download_folder = cons.DOWNLOAD_FOLDER, on_update=True, logger = logger)

# ESIOS = True
# KEST = True

if ESIOS and KEST:
    # Calcula los param A y B
    offer_maker.main_runner(date_inicial = date_today, mysql = mysql)
    logger.info("Cargados parámetros")
    # Calcula el índice para contratos indexados
    index_maker.main_runner(date_inicial = date_today, mysql = mysql)
    logger.info("Cargados índices")
else:
    logger.info("No se han calculado los parámetros por error de carga")
    send_mail(date.today())