from datetime import timedelta

from pandas import DataFrame
from utils.offer_tools import *
from constants import tarifas_ids, costes_comercializacion, dict_periodos

def main_runner(date_inicial, mysql):
    dict_cons = {
        "OS": get_const_OS(mysql),
        "OM": get_const_OM(mysql)
    }
    list_param = (793,794,797,798,799,800,802,803,1276,1285,1366)
    dt_periodos = DataFrame()
    dt_prices = DataFrame()
    dt_main = get_partial_data(date_inicial,"600",mysql)
    dt_main = dt_main.drop(["id_fixing_prices", "esiosid"], axis = 1)
    dt_main = dt_main.rename({'edate': 'day', 'ehour': 'hour', 'value': '600'}, axis = 1)

    for param in list_param:
        dt_main[str(param)] = list(get_single_param_data(date_inicial, param, mysql)["value"])

    list_tarifas_names = tarifas_ids.keys()
    list_tarifas_values = tarifas_ids.values()
    dt_main["SSAA"] = dt_main.apply(lambda row: calculate_ssaa(row), axis = 1)
    dt_main["KEST"] = get_kest(date_inicial, mysql)
    dt_main["OPER"] = dict_cons["OS"] + dict_cons["OM"]
    for name, value in zip(list_tarifas_names, list_tarifas_values):
        # if value != '14':
        pagos_capacidad_name = "PC_" + name
        dt_main[pagos_capacidad_name] = get_pagos_capacidad(date_inicial, value, mysql)
        coefperdidas_name = "perd " + name
        dt_main[coefperdidas_name] = get_single_param(date=date_inicial,param="coefperdidas",mysql=mysql,tarifa=value)["coefperdidas"].tolist()
        dt_main[("perd*Kest" + name)] = dt_main.apply(lambda row: calculate_perd_kest(row, name), axis = 1)
        dt_periodos[name] = get_periodos_tarifarios(date_inicial, name, mysql)[name].tolist()
    dt_prices["hour"] = dt_main["hour"]
    for name, value in zip(list_tarifas_names, list_tarifas_values):
        # if value != '14':
        dt_prices[name] = dt_main.apply(lambda row: calculate_price(row, name), axis = 1)
    list_tarifas2 = dt_prices.columns.values.tolist()
    list_tarifas2.remove("hour")
    for tarifa in list_tarifas2:
        upload_index(mysql = mysql, fecha = date_inicial, tarifa = tarifa,
                     list_values = dt_prices[tarifa].tolist(), list_hora=dt_prices["hour"].tolist())
