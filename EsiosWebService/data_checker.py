import smtplib, email, ssl
import pandas as pd
from datetime import timedelta

from email import encoders
from email.mime.base import MIMEBase
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText



def backup_saver(dt, fecha,fecha_fin, list_ids, logger, not_kest=False):
	if not_kest:
		dt.to_csv(path_or_buf="C:/Users/Diego/Documents/PWR_Params_AB/Backups/" + "backup_data_" +
	                             str(fecha.year) + "_" + str(fecha.month) + "_" + str(fecha.day) + ".csv")
	error = False
	date_list = pd.date_range(start = fecha, end = fecha_fin-timedelta(days=1))
	for day in date_list:
		dt_date = dt[dt["Date"] == str(day).split(" ")[0]]
		if dt_date.empty:
			logger.warn("Missing " + str(day))
			continue
		for id in list_ids:
			dt_id = dt_date[dt_date["Param"] == id]
			for i in range(1,25):
				if i not in dt_id["Hour"].tolist():
					logger.warn("Day " + str(day) + " Param " + id + " - Falta hora " + str(i))
					error = True
	return error

def send_mail(date):

	subject = "Carga " + str(date)
	body = "Log carga de Parámetros"
	sender_email = "dennison.jashan@opka.org"
	receiver_email = "diego.cuenca@nrgconsulting.es"
	password = "neenh4R?"

	message = MIMEMultipart()
	message["From"] = sender_email
	message["To"] = receiver_email
	message["Subject"] = subject
	message["Bcc"] = receiver_email

	message.attach(MIMEText(body, "plain"))

	filename = "Olivia_OfertMaker.log"
	filename2 = 'Logg_to_send.txt'

	with open(filename2, 'w') as the_file:
		with open(filename, 'r') as f:
			for line in f:
				d = line.split(" ", 1)[0]
				d= d.split("-")[0]
				if d == date:
					the_file.write(line)

	with open(filename2, "rb") as attachment:
		part = MIMEBase("application", "octet-stream")
		part.set_payload(attachment.read())

	encoders.encode_base64(part)

	part.add_header(
		"Content-Disposition",
		f"attachment; filename= {filename2}",
	)

	# message.attach(part)
	text = message.as_string()

	# Log in to server using secure context and send email
	context = ssl.create_default_context()
	with smtplib.SMTP_SSL("smtp.gmail.com", 465, context=context) as server:
		server.login(sender_email, password)
		server.sendmail(sender_email, receiver_email, text)