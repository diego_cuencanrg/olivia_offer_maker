from EsiosWebService import esios_web_functions as web_service
from managers.mysql_manager import MySQL_Manager
import constants as cons
import datetime
from datetime import date
import logging.config, logging
import offer_maker, index_maker
from utils.offer_tools import check_if_index
from EsiosWebService.data_checker import *

logging.config.fileConfig("logging.cfg")
logger = logging.getLogger("OliviaOfertMaker")
mysql = MySQL_Manager(dict_connection_values=cons.dict_values_DB, logger=logger)


def normal_launcher():
	date_today = date.today() - timedelta(days=1)
	date_before = date_today - timedelta(days=cons.DAYS_RANGE)
	ESIOS = ESIOS_Launcher(date_today, date_before)
	KEST = KEST_Launcher(date_today, date_before)

	if ESIOS and KEST:
		# Calcula los param A y B
		offer_maker.main_runner(date_inicial=date_today, mysql=mysql)
		logger.info("Cargados parámetros")
		# Calcula el índice para contratos indexados
		index_maker.main_runner(date_inicial=date_today, mysql=mysql)
		logger.info("Cargados índices")
		fill_index(date_before)
	else:
		logger.info("No se han calculado los parámetros por error de carga")

def jajaja_launcher():
	from managers.calendar_change import detect_calendar_change
	detect_calendar_change("2020-01-01", "2020-07-30")


def ESIOS_Launcher(date_before, date_today):
	return web_service.fixing_esios(mysql=mysql, start_date=date_before, end_date=date_today,
	                                list_ids=cons.ESIOS_PARAMS, on_update=True, logger=logger)


def KEST_Launcher(date_before, date_today):
	return web_service.fixing_kest(mysql=mysql, start_date=date_today,
	                               download_folder=cons.DOWNLOAD_FOLDER, on_update=True, logger=logger)


def fill_index(date_to_look):
	# Chequea que hay índices para los últimos 15 días y en caso contrario los calcula
	for i in range(1, cons.DAYS_RANGE + 1):
		search = check_if_index(date_to_look, mysql)
		if search != 24:
			index_maker.main_runner(date_inicial=date_to_look, mysql=mysql)
		date_to_look = date_to_look + timedelta(days=1)


def fill_params(date_before, date_to_look):
	date_before = datetime.datetime.strptime("2020-04-01", "%Y-%m-%d")
	date_to_look = date_before
	index_maker.main_runner(date_inicial=date_to_look, mysql=mysql)
	for i in range(1, 31):
		print(date_to_look)
		offer_maker.main_runner(date_inicial=date_to_look, mysql=mysql)
		print("Cargada info del " + str(date_to_look))
		date_to_look = date_to_look + timedelta(days=1)


normal_launcher()
jajaja_launcher(())