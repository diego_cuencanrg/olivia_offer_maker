from managers.mysql_manager import MySQL_Manager
import constants as const
from datetime import timedelta, date
from pandas import DataFrame
from utils.offer_tools import *



def main_runner(date_inicial, mysql, date_before):
	print("Iniciado proceso de cálculo")
	date_inicial = date_inicial - timedelta(days=1)
	dict_cons = {
		"OS": get_const_OS(mysql),
		"OM": get_const_OM(mysql)
	}

	get_const(mysql)
	dt_main = get_all_data(date_before, date_inicial, list_param="600", mysql=mysql)

	dt_2 = DataFrame()
	dt_2["day"] = list(dt_main["edate"])
	dt_2["hour"] = list(dt_main["ehour"])
	ESIOS_PARAMS = const.ESIOS_PARAMS
	if "1366" in ESIOS_PARAMS:
		ESIOS_PARAMS.remove("1366")

	for param in ESIOS_PARAMS:
		value_list = get_param_data(date_before, date_inicial, list_param=param, mysql=mysql)
		print(param)
		print(len(list(value_list["value"])))
		dt_2[param] = list(value_list["value"])

	# Load paramstruct DATA
	dt_2["Kest"] = get_param_data(date_before, date_inicial, list_param="999", mysql=mysql)

	# dt_2.to_csv("C:/Users/Diego Cuenca/Documents/Reportes Electricidad/Informe.csv", index=False)

	# Pagos por capacidad
	# dt_2["PC 2.0A"] = get_const_param(date_before, date_inicial, param="pagoscapacidad", mysql=mysql, tarifa=1)[
	# 	"pagoscapacidad"].tolist()
	# dt_2["PC 2.1A"] = get_const_param(date_before, date_inicial, param="pagoscapacidad", mysql=mysql, tarifa=2)[
	# 	"pagoscapacidad"].tolist()
	# dt_2["PC 2.0DHA"] = get_const_param(date_before, date_inicial, param="pagoscapacidad", mysql=mysql, tarifa=3)[
	# 	"pagoscapacidad"].tolist()
	# dt_2["PC 2.1DHA"] = get_const_param(date_before, date_inicial, param="pagoscapacidad", mysql=mysql, tarifa=4)[
	# 	"pagoscapacidad"].tolist()
	# dt_2["PC 2.0DHS"] = get_const_param(date_before, date_inicial, param="pagoscapacidad", mysql=mysql, tarifa=5)[
	# 	"pagoscapacidad"].tolist()
	# dt_2["PC 2.1DHS"] = get_const_param(date_before, date_inicial, param="pagoscapacidad", mysql=mysql, tarifa=6)[
	# 	"pagoscapacidad"].tolist()
	#
	# dt_2["PC 3.0A"] = get_const_param(date_before, date_inicial, param="pagoscapacidad", mysql=mysql, tarifa=7)[
	# 	"pagoscapacidad"].tolist()
	# dt_2["PC 3.1A"] = get_const_param(date_before, date_inicial, param="pagoscapacidad", mysql=mysql, tarifa=8)[
	# 	"pagoscapacidad"].tolist()
	# dt_2["PC 6"] = get_const_param(date_before, date_inicial, param="pagoscapacidad", mysql=mysql, tarifa=9)[
	# 	"pagoscapacidad"].tolist()

	dt_2["PC 2.0TD"] = get_const_param(date_before, date_inicial, param="pagoscapacidad", mysql=mysql, tarifa=14)[
		"pagoscapacidad"].tolist()
	dt_2["PC 3.0TD"] = get_const_param(date_before, date_inicial, param="pagoscapacidad", mysql=mysql, tarifa=15)[
		"pagoscapacidad"].tolist()
	dt_2["PC 6TD"] = get_const_param(date_before, date_inicial, param="pagoscapacidad", mysql=mysql, tarifa=16)[
		"pagoscapacidad"].tolist()

	# Coef de pérdidas
	# dt_2["perd 2.0A"] = get_const_param(date_before, date_inicial, param="coefperdidas", mysql=mysql, tarifa=1)[
	# 	"coefperdidas"].tolist()
	# dt_2["perd 2.1A"] = get_const_param(date_before, date_inicial, param="coefperdidas", mysql=mysql, tarifa=2)[
	# 	"coefperdidas"].tolist()
	# dt_2["perd 2.0DHA"] = get_const_param(date_before, date_inicial, param="coefperdidas", mysql=mysql, tarifa=3)[
	# 	"coefperdidas"].tolist()
	# dt_2["perd 2.1DHA"] = get_const_param(date_before, date_inicial, param="coefperdidas", mysql=mysql, tarifa=4)[
	# 	"coefperdidas"].tolist()
	# dt_2["perd 2.0DHS"] = get_const_param(date_before, date_inicial, param="coefperdidas", mysql=mysql, tarifa=5)[
	# 	"coefperdidas"].tolist()
	# dt_2["perd 2.1DHS"] = get_const_param(date_before, date_inicial, param="coefperdidas", mysql=mysql, tarifa=6)[
	# 	"coefperdidas"].tolist()
	#
	# dt_2["perd 3.0A"] = get_const_param(date_before, date_inicial, param="coefperdidas", mysql=mysql, tarifa=7)[
	# 	"coefperdidas"].tolist()
	# dt_2["perd 3.1A"] = get_const_param(date_before, date_inicial, param="coefperdidas", mysql=mysql, tarifa=8)[
	# 	"coefperdidas"].tolist()
	# dt_2["perd 6.1A"] = get_const_param(date_before, date_inicial, param="coefperdidas", mysql=mysql, tarifa=9)[
	# 	"coefperdidas"].tolist()
	# dt_2["perd 6.1B"] = get_const_param(date_before, date_inicial, param="coefperdidas", mysql=mysql, tarifa=10)[
	# 	"coefperdidas"].tolist()
	# dt_2["perd 6.2"] = get_const_param(date_before, date_inicial, param="coefperdidas", mysql=mysql, tarifa=11)[
	# 	"coefperdidas"].tolist()
	# dt_2["perd 6.3"] = get_const_param(date_before, date_inicial, param="coefperdidas", mysql=mysql, tarifa=12)[
	# 	"coefperdidas"].tolist()
	# dt_2["perd 6.4"] = get_const_param(date_before, date_inicial, param="coefperdidas", mysql=mysql, tarifa=13)[
	# 	"coefperdidas"].tolist()

	dt_2["perd 2.0TD"] = get_const_param(date_before, date_inicial, param="coefperdidas", mysql=mysql, tarifa=14)[
		"coefperdidas"].tolist()
	dt_2["perd 3.0TD"] = get_const_param(date_before, date_inicial, param="coefperdidas", mysql=mysql, tarifa=15)[
		"coefperdidas"].tolist()
	dt_2["perd 6.1TD"] = get_const_param(date_before, date_inicial, param="coefperdidas", mysql=mysql, tarifa=16)[
		"coefperdidas"].tolist()
	dt_2["perd 6.2TD"] = get_const_param(date_before, date_inicial, param="coefperdidas", mysql=mysql, tarifa=17)[
		"coefperdidas"].tolist()
	dt_2["perd 6.3TD"] = get_const_param(date_before, date_inicial, param="coefperdidas", mysql=mysql, tarifa=18)[
		"coefperdidas"].tolist()
	dt_2["perd 6.4TD"] = get_const_param(date_before, date_inicial, param="coefperdidas", mysql=mysql, tarifa=19)[
		"coefperdidas"].tolist()

	# Perd * K estimado
	# dt_2["perd*Kest 2.0A"] = list(dt_2["perd 2.0A"] * dt_2["Kest"])
	# dt_2["perd*Kest 2.1A"] = list(dt_2["perd 2.1A"] * dt_2["Kest"])
	# dt_2["perd*Kest 2.0DHA"] = list(dt_2["perd 2.0DHA"] * dt_2["Kest"])
	# dt_2["perd*Kest 2.1DHA"] = list(dt_2["perd 2.1DHA"] * dt_2["Kest"])
	# dt_2["perd*Kest 2.0DHS"] = list(dt_2["perd 2.0DHS"] * dt_2["Kest"])
	# dt_2["perd*Kest 2.1DHS"] = list(dt_2["perd 2.1DHS"] * dt_2["Kest"])
	#
	# dt_2["per*Kest 3.0A"] = list(dt_2["perd 3.0A"] * dt_2["Kest"])
	# dt_2["perd*Kest 3.1A"] = list(dt_2["perd 3.1A"] * dt_2["Kest"])
	# dt_2["perd*Kest 6.1A"] = list(dt_2["perd 6.1A"] * dt_2["Kest"])
	# dt_2["perd*Kest 6.1B"] = list(dt_2["perd 6.1B"] * dt_2["Kest"])
	# dt_2["perd*Kest 6.2"] = list(dt_2["perd 6.2"] * dt_2["Kest"])
	# dt_2["perd*Kest 6.3"] = list(dt_2["perd 6.3"] * dt_2["Kest"])
	# dt_2["perd*Kest 6.4"] = list(dt_2["perd 6.4"] * dt_2["Kest"])

	dt_2["perd*Kest 2.0TD"] = list(dt_2["perd 2.0TD"] * dt_2["Kest"])
	dt_2["perd*Kest 3.0TD"] = list(dt_2["perd 3.0TD"] * dt_2["Kest"])
	dt_2["perd*Kest 6.1TD"] = list(dt_2["perd 6.1TD"] * dt_2["Kest"])
	dt_2["perd*Kest 6.2TD"] = list(dt_2["perd 6.2TD"] * dt_2["Kest"])
	dt_2["perd*Kest 6.3TD"] = list(dt_2["perd 6.3TD"] * dt_2["Kest"])
	dt_2["perd*Kest 6.4TD"] = list(dt_2["perd 6.4TD"] * dt_2["Kest"])

	# Periodo Tarifario
	# dt_2["PT 2.0A"] = 1
	# dt_2["PT 2.1A"] = 1
	# dt_2["PT 2.0DHA"] = get_tarifas(date_before, date_inicial, mysql, "2.0DHA")
	# dt_2["PT 2.1DHA"] = get_tarifas(date_before, date_inicial, mysql, "2.1DHA")
	# dt_2["PT 2.0DHS"] = get_tarifas(date_before, date_inicial, mysql, "2.0DHS")
	# dt_2["PT 2.1DHS"] = get_tarifas(date_before, date_inicial, mysql, "2.1DHS")
	#
	# dt_2["PT 3.0A"] = get_tarifas(date_before, date_inicial, mysql, "3.0A")
	# dt_2["PT 3.1A"] = get_tarifas(date_before, date_inicial, mysql, "3.1A")
	# dt_2["PT 6"] = get_tarifas(date_before, date_inicial, mysql, "6.1A")

	dt_2["PT 3.0TD"] = get_tarifas(date_before, date_inicial, mysql, "3.0TD")
	dt_2["PT 2.0TD"] = get_tarifas(date_before, date_inicial, mysql, "2.0TD")
	dt_2["PT 6TD"] = get_tarifas(date_before, date_inicial, mysql, "6.1TD")

	# Parámetro A
	## NEW PARAMETRO A MARKET
	## Cambiar solo el nombre
	# dt_2["A_market 2.0A"] = dt_2.apply(lambda row: calculate_param_A_market(row, "2.0A", dict_cons), axis=1)
	# dt_2["A_market 2.1A"] = dt_2.apply(lambda row: calculate_param_A_market(row, "2.1A", dict_cons), axis=1)
	# dt_2["A_market 2.0DHA"] = dt_2.apply(lambda row: calculate_param_A_market(row, "2.0DHA", dict_cons), axis=1)
	# dt_2["A_market 2.1DHA"] = dt_2.apply(lambda row: calculate_param_A_market(row, "2.1DHA", dict_cons), axis=1)
	# dt_2["A_market 2.0DHS"] = dt_2.apply(lambda row: calculate_param_A_market(row, "2.0DHS", dict_cons), axis=1)
	# dt_2["A_market 2.1DHS"] = dt_2.apply(lambda row: calculate_param_A_market(row, "2.1DHS", dict_cons), axis=1)
	#
	# dt_2["A_market 3.0A"] = dt_2.apply(lambda row: calculate_param_A_market(row, "3.0A", dict_cons), axis=1)
	# dt_2["A_market 3.1A"] = dt_2.apply(lambda row: calculate_param_A_market(row, "3.1A", dict_cons), axis=1)
	# dt_2["A_market 6.1A"] = dt_2.apply(lambda row: calculate_param_A_market(row, "6.1A", dict_cons), axis=1)
	# dt_2["A_market 6.1B"] = dt_2.apply(lambda row: calculate_param_A_market(row, "6.1B", dict_cons), axis=1)
	# dt_2["A_market 6.2"] = dt_2.apply(lambda row: calculate_param_A_market(row, "6.2", dict_cons), axis=1)
	# dt_2["A_market 6.3"] = dt_2.apply(lambda row: calculate_param_A_market(row, "6.3", dict_cons), axis=1)
	# dt_2["A_market 6.4"] = dt_2.apply(lambda row: calculate_param_A_market(row, "6.4", dict_cons), axis=1)

	dt_2["A_market 2.0TD"] = dt_2.apply(lambda row: calculate_param_A_market(row, "2.0TD", dict_cons), axis=1)
	dt_2["A_market 3.0TD"] = dt_2.apply(lambda row: calculate_param_A_market(row, "3.0TD", dict_cons), axis=1)
	dt_2["A_market 6.1TD"] = dt_2.apply(lambda row: calculate_param_A_market(row, "6.1TD", dict_cons), axis=1)
	dt_2["A_market 6.2TD"] = dt_2.apply(lambda row: calculate_param_A_market(row, "6.2TD", dict_cons), axis=1)
	dt_2["A_market 6.3TD"] = dt_2.apply(lambda row: calculate_param_A_market(row, "6.3TD", dict_cons), axis=1)
	dt_2["A_market 6.4TD"] = dt_2.apply(lambda row: calculate_param_A_market(row, "6.4TD", dict_cons), axis=1)

	# Parámetro B
	## NEW Parámetro A (CAMBIAR CALCULO)
	# dt_2["A 2.0A"] = dt_2.apply(lambda row: calculate_param_A(row, "2.0A", dict_cons), axis=1)
	# dt_2["A 2.1A"] = dt_2.apply(lambda row: calculate_param_A(row, "2.1A", dict_cons), axis=1)
	# dt_2["A 2.0DHA"] = dt_2.apply(lambda row: calculate_param_A(row, "2.0DHA", dict_cons), axis=1)
	# dt_2["A 2.1DHA"] = dt_2.apply(lambda row: calculate_param_A(row, "2.1DHA", dict_cons), axis=1)
	# dt_2["A 2.0DHS"] = dt_2.apply(lambda row: calculate_param_A(row, "2.0DHS", dict_cons), axis=1)
	# dt_2["A 2.1DHS"] = dt_2.apply(lambda row: calculate_param_A(row, "2.1DHS", dict_cons), axis=1)
	#
	# dt_2["A 3.0A"] = dt_2.apply(lambda row: calculate_param_A(row, "3.0A", dict_cons), axis=1)
	# dt_2["A 3.1A"] = dt_2.apply(lambda row: calculate_param_A(row, "3.1A", dict_cons), axis=1)
	# dt_2["A 6.1A"] = dt_2.apply(lambda row: calculate_param_A(row, "6.1A", dict_cons), axis=1)
	# dt_2["A 6.1B"] = dt_2.apply(lambda row: calculate_param_A(row, "6.1A", dict_cons), axis=1)
	# dt_2["A 6.2"] = dt_2.apply(lambda row: calculate_param_A(row, "6.2", dict_cons), axis=1)
	# dt_2["A 6.3"] = dt_2.apply(lambda row: calculate_param_A(row, "6.3", dict_cons), axis=1)
	# dt_2["A 6.4"] = dt_2.apply(lambda row: calculate_param_A(row, "6.4", dict_cons), axis=1)

	dt_2["A 2.0TD"] = dt_2.apply(lambda row: calculate_param_A(row, "2.0TD", dict_cons), axis=1)
	dt_2["A 3.0TD"] = dt_2.apply(lambda row: calculate_param_A(row, "3.0TD", dict_cons), axis=1)
	dt_2["A 6.1TD"] = dt_2.apply(lambda row: calculate_param_A(row, "6.1TD", dict_cons), axis=1)
	dt_2["A 6.2TD"] = dt_2.apply(lambda row: calculate_param_A(row, "6.2TD", dict_cons), axis=1)
	dt_2["A 6.3TD"] = dt_2.apply(lambda row: calculate_param_A(row, "6.3TD", dict_cons), axis=1)
	dt_2["A 6.4TD"] = dt_2.apply(lambda row: calculate_param_A(row, "6.4TD", dict_cons), axis=1)

	# Parámetro B_market
	## New param B
	## cambiar solo el nombre
	# dt_2["B 2.0A"] = dt_2.apply(lambda row: calculate_param_B(row, "2.0A"), axis=1)
	# dt_2["B 2.1A"] = dt_2.apply(lambda row: calculate_param_B(row, "2.1A"), axis=1)
	# dt_2["B 2.0DHA"] = dt_2.apply(lambda row: calculate_param_B(row, "2.0DHA"), axis=1)
	# dt_2["B 2.1DHA"] = dt_2.apply(lambda row: calculate_param_B(row, "2.1DHA"), axis=1)
	# dt_2["B 2.0DHS"] = dt_2.apply(lambda row: calculate_param_B(row, "2.0DHS"), axis=1)
	# dt_2["B 2.1DHS"] = dt_2.apply(lambda row: calculate_param_B(row, "2.1DHS"), axis=1)
	#
	# dt_2["B 3.0A"] = dt_2.apply(lambda row: calculate_param_B(row, "3.0A"), axis=1)
	# dt_2["B 3.1A"] = dt_2.apply(lambda row: calculate_param_B(row, "3.1A"), axis=1)
	# dt_2["B 6.1A"] = dt_2.apply(lambda row: calculate_param_B(row, "6.1A"), axis=1)
	# dt_2["B 6.1B"] = dt_2.apply(lambda row: calculate_param_B(row, "6.1B"), axis=1)
	# dt_2["B 6.2"] = dt_2.apply(lambda row: calculate_param_B(row, "6.2"), axis=1)
	# dt_2["B 6.3"] = dt_2.apply(lambda row: calculate_param_B(row, "6.3"), axis=1)
	# dt_2["B 6.4"] = dt_2.apply(lambda row: calculate_param_B(row, "6.4"), axis=1)

	dt_2["B 2.0TD"] = dt_2.apply(lambda row: calculate_param_B(row, "2.0TD"), axis=1)
	dt_2["B 3.0TD"] = dt_2.apply(lambda row: calculate_param_B(row, "3.0TD"), axis=1)
	dt_2["B 6.1TD"] = dt_2.apply(lambda row: calculate_param_B(row, "6.1TD"), axis=1)
	dt_2["B 6.2TD"] = dt_2.apply(lambda row: calculate_param_B(row, "6.2TD"), axis=1)
	dt_2["B 6.3TD"] = dt_2.apply(lambda row: calculate_param_B(row, "6.3TD"), axis=1)
	dt_2["B 6.4TD"] = dt_2.apply(lambda row: calculate_param_B(row, "6.4TD"), axis=1)

	dt_2.to_csv("C:/Users/Diego/Documents/Reportes Electricidad/Informe.csv", index=False)

	today = date.today()

	if date_before.day == 1:
		month = True
		month_date = date_before
	else:
		month = False
		month_date = date_before
	calculate_offers(dt_2, mysql, today, month, month_date)