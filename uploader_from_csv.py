import csv
from EsiosWebService import esios_web_functions as web_service
from managers.mysql_manager import MySQL_Manager
import constants as cons
import datetime
from datetime import timedelta, date
import logging.config,logging
import offer_maker, index_maker
from EsiosWebService.data_checker import *

logging.config.fileConfig("logging.cfg")
logger = logging.getLogger("OliviaOfertMaker")
mysql = MySQL_Manager(dict_connection_values= cons.dict_values_DB, logger = logger)

with open("Libro1.csv") as csvfile:
	readCSV = csv.reader(csvfile, delimiter = ';')
	for row in readCSV:
		print(row)
		sql = '''INSERT INTO `power_retailer`.`fixing_prices`
		(`esiosid`,
		`edate`,
		`ehour`,
		`value`)
		VALUES
		(1366,
		"{FECHA}",
		{HORA},
		{VALOR});
		'''.format(FECHA = row[2], HORA = row[3], VALOR = row[1])
		data = list()
		mysql.execute_procedure(query = sql, data = data, to_dataframe=False)


