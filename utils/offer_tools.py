import constants as const
import json
import csv
import openpyxl
import copy
# from sender import EmailSender

def get_const(mysql):
	sql = '''SELECT name,value FROM power_retailer.constants; '''

	data = list()
	cursor = mysql.execute_procedure(query=sql, data=data, to_dataframe=False)

	const.data_om = cursor._result.rows[0][1]
	const.data_os = cursor._result.rows[1][1]


def get_all_data(date_inicial, date_final, list_param, mysql):
	sql = '''SELECT * FROM power_retailer.fixing_prices 
			WHERE 
			esiosid in ({LISTADO}) and edate between "{FECHA_INICIO}" and "{FECHA_FIN}"
			order by esiosid,edate,ehour asc;'''.format(LISTADO=list_param, FECHA_INICIO=date_inicial,
	                                                    FECHA_FIN=date_final)

	data = list()
	cursor = mysql.execute_procedure(query=sql, data=data, to_dataframe=True)

	return cursor

def get_partial_data(date_final, list_param, mysql):
	sql = '''SELECT * FROM power_retailer.fixing_prices
			WHERE
			esiosid in ({LISTADO}) and edate = "{DATE}" order by esiosid, edate, ehour asc;
	'''.format(LISTADO = list_param, DATE = date_final)
	data = list()
	cursor = mysql.execute_procedure(query = sql, data = data, to_dataframe= True)
	return cursor

def get_pagos_capacidad(date, tarifa, mysql):
	sql = '''SELECT pagoscapacidad from power_retailer.paramstruct
	 where psdate = "{FECHA}" and tarifa = {TARIFA} ;'''.format(FECHA = date, TARIFA = tarifa)
	data = list()
	cursor = mysql.execute_procedure(query=sql, data = data, to_dataframe=True)["pagoscapacidad"].tolist()
	return cursor

def getPool(mysql):
	query = '''SELECT  * FROM (
						(Select edate, ehour, Value from power_retailer.fixing_prices where esiosid = 600 and edate > "2017-12-31") tabla1
						JOIN
						(Select * from power_retailer.lista_tarifas where Fecha > "2017-12-31") tabla2
						ON tabla1.edate = tabla2.Fecha AND tabla1.ehour = tabla2.Hora);
					'''
	data = list()
	cursor = mysql.execute_procedure(query = query, data = data, to_dataframe=True)
	return cursor

def get_tarifas(date_inicial, date_final, mysql, tarifa):
	sql = '''SELECT fecha,hora,`{TARIFA}` from power_retailer.lista_tarifas 
	where fecha between "{FECHA_INICIO}" and "{FECHA_FINAL}" order by fecha,hora asc;'''.format(TARIFA = tarifa, FECHA_INICIO = date_inicial, FECHA_FINAL = date_final)

	data = list()
	cursor = mysql.execute_procedure(query=sql, data=data, to_dataframe=True)
	return cursor[tarifa].tolist()

def get_periodos_tarifarios(date, tarifa, mysql):
	sql = '''SELECT fecha,hora,`{TARIFA}` from power_retailer.lista_tarifas 
			where fecha = "{FECHA_INICIO}" 
			order by fecha,hora asc;'''.format(TARIFA=tarifa, FECHA_INICIO=date)
	data = list()
	cursor = mysql.execute_procedure(query = sql, data = data, to_dataframe=True)
	return cursor


def get_kest(date, mysql):
	sql = '''SELECT edate, ehour, value FROM power_retailer.fixing_prices 
				WHERE 
				esiosid = "999" and edate = "{FECHA}"
				order by esiosid,edate,ehour asc;'''.format(FECHA = date)
	data = list()
	cursor = mysql.execute_procedure(query=sql, data=data, to_dataframe=True)
	cursor = cursor.drop(["edate", "ehour"], axis=1)
	return cursor


def get_param_data(date_inicial, date_final, list_param, mysql):
	sql = '''SELECT edate, ehour, value FROM power_retailer.fixing_prices 
			WHERE 
			esiosid = "{LISTADO}" and edate between "{FECHA_INICIO}" and "{FECHA_FIN}"
			order by esiosid,edate,ehour asc;'''.format(LISTADO=list_param, FECHA_INICIO=date_inicial,
	                                                    FECHA_FIN=date_final)
	data = list()
	cursor = mysql.execute_procedure(query=sql, data=data, to_dataframe=True)
	cursor = cursor.drop(["edate","ehour"], axis = 1)
	# if '999' in list_param:
	# 	cursor.drop(cursor.tail(23).index,inplace=True)
	return cursor

def get_single_param_data(date, list_param, mysql):
	sql = '''SELECT edate, ehour, value FROM power_retailer.fixing_prices 
				WHERE 
				esiosid = "{LISTADO}" and edate = "{FECHA_INICIO}"
				order by esiosid,edate,ehour asc;'''.format(LISTADO=list_param, FECHA_INICIO=date)
	data = list()
	cursor = mysql.execute_procedure(query=sql, data=data, to_dataframe=True)
	cursor = cursor.drop(["edate", "ehour"], axis=1)
	return cursor

def get_const_param(date_inicial, date_final, param, mysql, tarifa):
	sql = '''SELECT {PARAM} FROM power_retailer.paramstruct 
				WHERE 
				tarifa = {TARIFA} and psdate between "{FECHA_INICIO}" and "{FECHA_FIN}"
				order by psdate,pshour asc;'''.format(PARAM = param, TARIFA = tarifa,
	                                                  FECHA_INICIO = date_inicial, FECHA_FIN = date_final)
	data = list()
	cursor = mysql.execute_procedure(query=sql, data=data, to_dataframe=True)
	# if param in ('pagoscapacidad',"coefperdidas"):
	# 	cursor.drop(cursor.tail(23).index,inplace=True)
	return cursor

def get_single_param(date, param, mysql, tarifa):
	sql = '''SELECT {PARAM} FROM power_retailer.paramstruct
			WHERE
			tarifa = {TARIFA} and psdate = "{FECHA}"
			order by psdate, pshour asc;'''.format(PARAM = param, TARIFA = tarifa, FECHA = date)
	data = list()
	cursor = mysql.execute_procedure(query = sql, data = data, to_dataframe=True)
	return cursor

def get_const_OM(mysql):
	sql = '''SELECT Value FROM power_retailer.constants
	        WHERE
	        Name = "OM";'''
	data = list()
	cursor = mysql.execute_procedure(query = sql, data = data, to_dataframe=True)
	return cursor.iloc[0].iloc[0]

def get_const_OS(mysql):
	sql = '''SELECT Value FROM power_retailer.constants
			WHERE
			Name = "OS";'''
	data = list()
	cursor = mysql.execute_procedure(query = sql, data = data, to_dataframe= True)
	return cursor.iloc[0].iloc[0]

def calculate_param_A_market(row, tarifa, dict_const):
	dict_valores = {}
	if tarifa == "3.0A":
		dict_valores = const.tarifa3A
	elif tarifa == "3.1A":
		dict_valores = const.tarifa31A
	elif tarifa == "6.1A":
		dict_valores = const.tarifa61A
	elif tarifa == "6.1B":
		dict_valores =  const.tarifa61B
	elif tarifa == "6.2":
		dict_valores = const.tarifa62
	elif tarifa == "6.3":
		dict_valores = const.tarifa63
	elif tarifa == "6.4":
		dict_valores = const.tarifa64
	elif tarifa == "2.0A":
		dict_valores = const.tarifa20A
	elif tarifa == "2.1A":
		dict_valores = const.tarifa21A
	elif tarifa == "2.0DHA":
		dict_valores = const.tarifa20DHA
	elif tarifa == "2.1DHA":
		dict_valores = const.tarifa21DHA
	elif tarifa == "2.0DHS":
		dict_valores = const.tarifa20DHS
	elif tarifa == "2.1DHS":
		dict_valores = const.tarifa21DHS

	# Nuevas Tarifas

	elif tarifa == "3.0TD":
		dict_valores = const.tarifa30T
	elif tarifa == "2.0TD":
		dict_valores = const.tarifa20T
	elif tarifa == "6.1TD":
		dict_valores = const.tarifa61TD
	elif tarifa == "6.2TD":
		dict_valores = const.tarifa62TD
	elif tarifa == "6.3TD":
		dict_valores = const.tarifa63TD
	elif tarifa == "6.4TD":
		dict_valores = const.tarifa64TD

	data_om = dict_const["OM"]
	data_os = dict_const["OS"]

	param_A1_market = row["1276"] + row[dict_valores["pagos_capacidad"]] + row["798"] + row["797"] + row["793"] + row["794"] + row["796"] + row["1285"] + row["803"] + row["799"] + row["800"] + row["802"]
	param_A1_market = float(param_A1_market) + data_om + data_os
	param_A2_market = 1 + (row[dict_valores["perdidaK"]])
	param_A3_market = const.costes_gestion * const.TASA_MUNICIPAL
	# Anteriormente param_A3 = const.costes_gestion * 1.05
	paramA_market = float(param_A1_market) * float(param_A2_market) * const.TASA_MUNICIPAL + param_A3_market
	return paramA_market

def calculate_param_A(row, tarifa,dict_const):
	dict_valores = {}
	if tarifa == "3.0A":
		dict_valores = const.tarifa3A
	elif tarifa == "3.1A":
		dict_valores = const.tarifa31A
	elif tarifa == "6.1A":
		dict_valores = const.tarifa61A
	elif tarifa == "6.1B":
		dict_valores = const.tarifa61B
	elif tarifa == "6.2":
		dict_valores = const.tarifa62
	elif tarifa == "6.3":
		dict_valores = const.tarifa63
	elif tarifa == "6.4":
		dict_valores = const.tarifa64
	elif tarifa == "2.0A":
		dict_valores = const.tarifa20A
	elif tarifa == "2.1A":
		dict_valores = const.tarifa21A
	elif tarifa == "2.0DHA":
		dict_valores = const.tarifa20DHA
	elif tarifa == "2.1DHA":
		dict_valores = const.tarifa21DHA
	elif tarifa == "2.0DHS":
		dict_valores = const.tarifa20DHS
	elif tarifa == "2.1DHS":
		dict_valores = const.tarifa21DHS

	# Nuevas Tarifas
	elif tarifa == "2.0TD":
		dict_valores = const.tarifa20T
	elif tarifa == "3.0TD":
		dict_valores = const.tarifa30T
	elif tarifa == "6.1TD":
		dict_valores = const.tarifa61TD
	elif tarifa == "6.2TD":
		dict_valores = const.tarifa62TD
	elif tarifa == "6.3TD":
		dict_valores = const.tarifa63TD
	elif tarifa == "6.4TD":
		dict_valores = const.tarifa64TD

	data_om = dict_const["OM"]
	data_os = dict_const["OS"]

	param_A1 = row["1276"] + row[dict_valores["pagos_capacidad"]] + row["798"] + row["797"] + row["793"] + row[
		"794"] + row["796"] + row["1285"] + row["803"] + row["799"] + row["800"] + row["802"]
	param_A1 = float(param_A1) + data_om + data_os
	param_A2 = 1 + (row[dict_valores["perdidaK"]])
	param_A3 = const.costes_gestion * const.TASA_MUNICIPAL
	param_A4_tmp = (row["764"] - row["600"])
	param_A4 = 0 if param_A4_tmp < 0 else param_A4_tmp
	paramA = (float(param_A1) + float(param_A4)) * float(param_A2) * const.TASA_MUNICIPAL + param_A3
	return paramA

def calculate_param_B(row,tarifa):
	dict_valores = {}
	if tarifa == "3.0A":
		dict_valores = const.tarifa3A
	elif tarifa == "3.1A":
		dict_valores = const.tarifa31A
	elif tarifa == "6.1A":
		dict_valores = const.tarifa61A
	elif tarifa == "6.1B":
		dict_valores = const.tarifa61B
	elif tarifa == "6.2":
		dict_valores = const.tarifa62
	elif tarifa == "6.3":
		dict_valores = const.tarifa63
	elif tarifa == "6.4":
		dict_valores = const.tarifa64
	elif tarifa == "2.0A":
		dict_valores = const.tarifa20A
	elif tarifa == "2.1A":
		dict_valores = const.tarifa21A
	elif tarifa == "2.0DHA":
		dict_valores = const.tarifa20DHA
	elif tarifa == "2.1DHA":
		dict_valores = const.tarifa21DHA
	elif tarifa == "2.0DHS":
		dict_valores = const.tarifa20DHS
	elif tarifa == "2.1DHS":
		dict_valores = const.tarifa21DHS

	# Nuevas Tarifas
	elif tarifa == "2.0TD":
		dict_valores = const.tarifa20T
	elif tarifa == "3.0TD":
		dict_valores = const.tarifa30T
	elif tarifa == "6.1TD":
		dict_valores = const.tarifa61TD
	elif tarifa == "6.2TD":
		dict_valores = const.tarifa62TD
	elif tarifa == "6.3TD":
		dict_valores = const.tarifa63TD
	elif tarifa == "6.4TD":
		dict_valores = const.tarifa64TD

	paramB_market = float((1 + row[dict_valores["perdidaK"]])) * const.TASA_MUNICIPAL

	return paramB_market

def upload_offers(mysql, dict_valores, fecha):
	sql = '''INSERT INTO power_retailer.ofertas_param(fecha,AMarket,A,B,tarifa,periodo)
	VALUES("{FECHA}",{A_MARKET},{A},{B},"{TARIFA}",{PERIODO}); '''

	for tarifa in dict_valores:
		for periodo in dict_valores[tarifa]:
			if dict_valores[tarifa][periodo]:
				sql2 = sql.format(FECHA=fecha,
				                  A_MARKET=dict_valores[tarifa][periodo]["A_market"],
				                  A=dict_valores[tarifa][periodo]["A"],
				                  B = dict_valores[tarifa][periodo]["B"],
				                  TARIFA = tarifa,
				                  PERIODO = periodo)
				data = list()
				mysql.execute_procedure(query=sql2, data=data, to_dataframe=False)


def upload_offers_month(mysql, dict_valores,month):
	sql = '''INSERT INTO power_retailer.ofertas_param_month(month,year,AMarket,A,B,tarifa,periodo)
	VALUES({MONTH},{YEAR},{A_MARKET},{A},{B},"{TARIFA}",{PERIODO}); '''

	for tarifa in dict_valores:
		for periodo in dict_valores[tarifa]:
			if dict_valores[tarifa][periodo]:
				sql2 = sql.format(MONTH=month.month,
				                  YEAR=month.year,
				                  A_MARKET=dict_valores[tarifa][periodo]["A_market"],
				                  A=dict_valores[tarifa][periodo]["A"],
				                  B = dict_valores[tarifa][periodo]["B"],
				                  TARIFA = tarifa,
				                  PERIODO = periodo)
				data = list()
				mysql.execute_procedure(query=sql2, data=data, to_dataframe=False)


def upload_index(mysql, list_values, fecha, tarifa,list_hora):
	for valor, hora in zip(list_values, list_hora):
		sql = '''INSERT INTO `power_retailer`.`index_param` 
			(`fecha`,`tarifa`,`hora`,`value`)
			VALUES
			("{FECHA}","{TARIFA}",{HORA},{VALOR})
			ON DUPLICATE KEY UPDATE value = VALUES(value);'''.format(FECHA=fecha,TARIFA=tarifa,
		                                                     HORA=hora,VALOR=valor)
		data = list()
		mysql.execute_procedure(query = sql, data = data, to_dataframe= False)

def calculate_offers(dt, mysql, fecha, month = False, month_date = ""):

	# list_tarifas = ("2.0A","2.1A","2.0DHA","2.1DHA","2.0DHS","2.1DHS","3.0A", "3.1A", "6.1A", "6.1B", "6.2", "6.3", "6.4","2.0TD","3.0TD", "6.1TD", "6.2TD", "6.3TD", "6.4TD")
	list_tarifas = ("2.0TD", "3.0TD", "6.1TD", "6.2TD", "6.3TD", "6.4TD")
	list_periodos = (1,2,3,4,5,6)
	# dict_resuls = {
	# 	"2.0A":{"1": {}, "2": {}, "3": {}, "4":{}, "5":{}, "6":{}},
	# 	"2.1A":{"1": {}, "2": {}, "3": {}, "4":{}, "5":{}, "6":{}},
	# 	"2.0DHA":{"1": {}, "2": {}, "3": {}, "4":{}, "5":{}, "6":{}},
	# 	"2.1DHA":{"1": {}, "2": {}, "3": {}, "4":{}, "5":{}, "6":{}},
	# 	"2.0DHS":{"1": {}, "2": {}, "3": {}, "4":{}, "5":{}, "6":{}},
	# 	"2.1DHS":{"1": {}, "2": {}, "3": {}, "4":{}, "5":{}, "6":{}},
	# 	"3.0A":{"1": {}, "2": {}, "3": {}, "4":{}, "5":{}, "6":{}},
	# 	"3.1A":{"1": {}, "2": {}, "3": {}, "4":{}, "5":{}, "6":{}},
	# 	"6.1A":{"1": {}, "2": {}, "3": {}, "4":{}, "5":{}, "6":{}},
	# 	"6.1B":{"1": {}, "2": {}, "3": {}, "4":{}, "5":{}, "6":{}},
	# 	"6.2":{"1": {}, "2": {}, "3": {}, "4":{}, "5":{}, "6":{}},
	# 	"6.3":{"1": {}, "2": {}, "3": {}, "4":{}, "5":{}, "6":{}},
	# 	"6.4":{"1": {}, "2": {}, "3": {}, "4":{}, "5":{}, "6":{}},
	# 	"2.0TD": {"1": {}, "2": {}, "3": {}, "4": {}, "5": {}, "6": {}},
	# 	"3.0TD":{"1": {}, "2": {}, "3": {}, "4":{}, "5":{}, "6":{}},
	# 	"6.1TD":{"1": {}, "2": {}, "3": {}, "4":{}, "5":{}, "6":{}},
	# 	"6.2TD":{"1": {}, "2": {}, "3": {}, "4":{}, "5":{}, "6":{}},
	# 	"6.3TD":{"1": {}, "2": {}, "3": {}, "4":{}, "5":{}, "6":{}},
	# 	"6.4TD":{"1": {}, "2": {}, "3": {}, "4":{}, "5":{}, "6":{}}
	# }
	dict_resuls = {
		"2.0TD": {"1": {}, "2": {}, "3": {}, "4": {}, "5": {}, "6": {}},
		"3.0TD": {"1": {}, "2": {}, "3": {}, "4": {}, "5": {}, "6": {}},
		"6.1TD": {"1": {}, "2": {}, "3": {}, "4": {}, "5": {}, "6": {}},
		"6.2TD": {"1": {}, "2": {}, "3": {}, "4": {}, "5": {}, "6": {}},
		"6.3TD": {"1": {}, "2": {}, "3": {}, "4": {}, "5": {}, "6": {}},
		"6.4TD": {"1": {}, "2": {}, "3": {}, "4": {}, "5": {}, "6": {}}
	}

	for tarifa in list_tarifas:
		for periodo in list_periodos:
			if periodo > 1 and tarifa in ("2.0A","2.1A"):
				continue
			if periodo > 2 and tarifa in ("2.0DHA","2.1DHA"):
				continue
			if periodo > 3 and tarifa in ("3.0A", "3.1A", "2.0DHS", "2.1DHS", "2.0TD"):
				continue
			if tarifa in ("2.0A","2.1A","2.0DHA","2.1DHA","2.0DHS","2.1DHS","3.0A", "3.1A"):
				column = "PT " + tarifa
			else:
				if tarifa in ("6.1A","6.1B","6.2","6.3","6.4"):
					column = "PT 6"
				else:
					if tarifa in "3.0TD":
						column = "PT 3.0TD"
					elif tarifa in "2.0TD":
						column = "PT 2.0TD"
					else:
						column = "PT 6TD"

			columnA_market = "A_market " + tarifa
			columnA = "A " + tarifa
			columnB = "B " + tarifa
			dt_to_calculate = dt[dt[column] == periodo]

			paramA_market_list =  [x for x in dt_to_calculate[columnA_market].tolist() if str(x) != 'nan']
			paramA_list =  [x for x in dt_to_calculate[columnA].tolist() if str(x) != 'nan']
			paramB_list = [x for x in dt_to_calculate[columnB].tolist() if str(x) != 'nan']

			paramA_market = sum(paramA_market_list) / len(paramA_market_list) if len(paramA_market_list) >= 1 else 0
			paramA = sum(paramA_list) / len(paramA_list) if len(paramA_list) >= 1 else 0
			paramB = sum (paramB_list) / len(paramB_list) if len(paramB_list) >= 1 else 0

			dict_resuls[tarifa][str(periodo)]["A_market"] = paramA_market
			dict_resuls[tarifa][str(periodo)]["A"] = paramA
			dict_resuls[tarifa][str(periodo)]["B"] = paramB

	if month:
		json_name = const.REPORT_FOLDER + "ParamsAB_{}_{}.json".format(month_date.month,month_date.year)
		upload_offers_month(mysql=mysql, dict_valores=dict_resuls, month= month_date)
		excel_file = dump_to_excel(dict_resuls, json_name)
		# send_to_mail(excel_file)
	else:
		json_name = const.REPORT_FOLDER + "ParamsAB_{:%Y%m%d}.json".format(fecha)
		upload_offers(mysql=mysql, dict_valores=dict_resuls, fecha=fecha)

	with open(json_name, "w") as outfile:
		json.dump(dict_resuls, outfile)


def calculate_ssaa(row):
	ssaa = float(row["793"]+row["794"]+row["797"]+row["798"]+row["799"]+row["800"]+row["802"]+
	             row["803"]+row["1276"]+row["1285"]+row["1366"])
	return ssaa

def calculate_perd_kest(row, tarifa):
	coef_name = "perd " + tarifa
	perdkest = float(row[coef_name] * row["KEST"])
	return perdkest

def calculate_price(row, tarifa):
	price = ((float(row["600"]) + row["SSAA"] + float(row[("PC_"+tarifa)]) + row["OPER"]) * (1 + row[("perd*Kest")+tarifa]))
	return price

def check_if_index(date_to_look, mysql):
	sql = '''SELECT * FROM power_retailer.index_param 
	where tarifa = "2.0A" and fecha = "{FECHA}";'''.format(FECHA = date_to_look)

	data = list()
	cursor = mysql.execute_procedure(query=sql, data = data, to_dataframe=False)
	return cursor.rowcount

def dump_to_csv(resuls, name):
	row_titles = ['P1', 'P2', 'P3', 'P4', 'P5', 'P6']
	row_tarifas = ['2.0TD', '3.0TD', '6.1TD', '6.2TD', '6.3TD', '6.4TD']

	name = name[:-4] + 'csv'

	with open(name, 'w', newline='') as file:
		writer = csv.writer(file)
		writer.writerow((['A_market'] + row_titles))
		for r in row_tarifas:
			row_temp = []
			for t in row_titles:
				if 'A_market' in resuls[r][t.replace('P','')].keys():
					row_temp.append(resuls[r][t.replace('P','')]['A_market'])
			writer.writerow([r] + row_temp)
		writer.writerow([])
		writer.writerow(((['B_market'] + row_titles)))
		for r in row_tarifas:
			row_temp = []
			for t in row_titles:
				if 'B' in resuls[r][t.replace('P','')].keys():
					row_temp.append(resuls[r][t.replace('P','')]['B'])
			writer.writerow([r] + row_temp)
		writer.writerow([])
		writer.writerow(((['A_dev'] + row_titles)))
		for r in row_tarifas:
			row_temp = []
			for t in row_titles:
				if 'A' in resuls[r][t.replace('P','')].keys():
					row_temp.append(resuls[r][t.replace('P','')]['A'])
			writer.writerow([r] + row_temp)

	return name

def dump_to_excel(resuls, name):
	row_titles = ['P1', 'P2', 'P3', 'P4', 'P5', 'P6']
	row_tarifas = ['2.0TD', '3.0TD', '6.1TD', '6.2TD', '6.3TD', '6.4TD']

	name = name[:-4] + 'xlsx'

	wb = openpyxl.Workbook()
	ws = wb.active

	ws.title = 'A_market'
	ws2 = wb.create_sheet('B_market')
	ws3 = wb.create_sheet('A_dev')
	i, j = 1, 1
	for t in row_titles:
		ws.cell(row=i, column=j, value=t)
		ws2.cell(row=i, column=j, value=t)
		ws3.cell(row=i, column=j, value=t)
		j += 1
	A_m_rows, B_m_rows, A_dev_rows = [], [], []
	for tarifa in row_tarifas:
		temp1, temp2, temp3 = [], [], []
		for t in row_titles:
			if 'A_market' in resuls[tarifa][t.replace('P', '')].keys():
				temp1.append(resuls[tarifa][t.replace('P', '')]['A_market'])
			if 'B' in resuls[tarifa][t.replace('P', '')].keys():
				temp2.append(resuls[tarifa][t.replace('P', '')]['B'])
			if 'A' in resuls[tarifa][t.replace('P', '')].keys():
				temp3.append(resuls[tarifa][t.replace('P', '')]['A'])
		A_m_rows.append(temp1)
		B_m_rows.append(temp2)
		A_dev_rows.append(temp3)
	for row in A_m_rows:
		ws.append(row)
	for row in B_m_rows:
		ws2.append(row)
	for row in A_dev_rows:
		ws3.append(row)
	wb.save(name)
	return name


def send_to_mail(file):
	contacts = []
	sender = EmailSender()
	sender._init_connection()
	sender.send_email(contacts, 'Parámetros mensuales', attachment_path_enrg=file)

	pass