
import os
import time
import datetime

import bs4
import xlrd
from selenium import webdriver

class WebpageNotAviable(Exception):
    pass

def update_prices(self):
    self.logger.info('eSIOS fixing <Other markets> values')
    cursor = self.mysql.execute_procedure(query="SELECT esiosid FROM esios_ids WHERE tipo = %s;", data=[1])
    l_ids = []
    for  result in cursor:
        l_ids.append(result[0])
    for ids in l_ids:
        cursor = self.mysql.execute_procedure(query="SELECT MAX(edate) FROM fixing_prices WHERE esiosid = %s;", data=[ids])
        max_price_date = cursor.fetchone()
        if max_price_date[0] == None:
            max_price_date = datetime.date(2014,1,1)
        else:
            max_price_date = max_price_date[0] + datetime.timedelta(days=-15)
        if self.trading_date >= max_price_date:
            self.logger.info('Updating ID {} values for dates beyond {}'.format(ids, max_price_date))
            self.web_data_serice.upload_esios_fixing(
                idx=ids,
                start_date=max_price_date,
                end_date=self.trading_date,
                on_update=True
            )

def update_ht_prices(self):
    self.logger.info('eSIOS fixing <Other markets> values')
    cursor = self.mysql.execute_procedure(query="SELECT esiosid FROM esios_ids WHERE hueco_termico = %s;", data=[1])
    l_ids = []
    for result in cursor:
        l_ids.append(result[0])
    for ids in l_ids:
        cursor = self.mysql.execute_procedure(query="SELECT MAX(edate) FROM fixing_prices WHERE esiosid = %s;",data=[ids])
        max_price_date = cursor.fetchone()
        if max_price_date[0] == None:
            max_price_date = datetime.date(2016, 1, 1)
        else:
            max_price_date = max_price_date[0] + datetime.timedelta(days=-15)

        if self.trading_date >= max_price_date:
            self.web_data_serice.upload_esios_fixing(
                idx=ids,
                start_date=max_price_date,
                end_date=self.trading_date,
                on_update=True
            )

def update_kest(self):
    self.logger.info('eSIOS fixing <kest> values')
    self.web_data_serice.upload_kest_fixing(start_date=self.trading_date, on_update=True)

def update_spot(self):
    self.logger.info('eSIOS fixing <spot> values')
    cursor = self.mysql.execute_procedure(query="SELECT MAX(edate) FROM fixing_spot;", data=[])
    max_price_date = cursor.fetchone()
    if max_price_date[0] == None:
        max_price_date = datetime.date(2014, 1, 1)
    else:
        max_price_date = max_price_date[0] + datetime.timedelta(days=-15)
    if self.trading_date >= max_price_date:
        self.web_data_serice.upload_esios_spot(
            start_date=max_price_date,
            end_date=self.trading_date,
            on_update=True
        )

def update_contracts(self):
    import math
    def parse_dates(contract):

        from calendar import monthrange

        if 'FTB YR' in contract:
            iyear = int(contract.split('FTB YR-')[1]) + 2000
            return (datetime.date(iyear, 1, 1), datetime.date(iyear, 12, 31), 'Y', iyear)

        if 'FTB Q' in contract:
            q = (contract.split('FTB Q')[1]).split('-')
            iyear = int(q[1]) + 2000
            m = []
            if int(q[0]) == 1:
                m.append(1)
                m.append(3)
            if int(q[0]) == 2:
                m.append(4)
                m.append(6)
            if int(q[0]) == 3:
                m.append(7)
                m.append(9)
            if int(q[0]) == 4:
                m.append(10)
                m.append(12)

            m.append(monthrange(iyear, m[1])[1])
            return (datetime.date(iyear, m[0], 1), datetime.date(iyear, m[1], m[2]), 'Q', iyear, int(q[0]))

        if 'FTB Wk' in contract:
            wk = (contract.split('FTB Wk')[1]).split('-')
            iyear = int(wk[1]) + 2000
            d = str(iyear) + '-W' + wk[0]
            d1 = datetime.datetime.strptime(d + '-1', "%Y-W%W-%w")
            d2 = d1 + datetime.timedelta(days=6)
            return (d1.date(), d2.date(), 'W')

        if 'FTB M' in contract:
            m = (contract.split('FTB M ')[1]).split('-')
            imonth = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'].index(m[0]) + 1
            iyear = int(m[1]) + 2000
            iquarter = math.ceil(float(imonth) / 3)
            return (datetime.date(iyear, imonth, 1), datetime.date(iyear, imonth, monthrange(iyear, imonth)[1]), 'M', iyear, iquarter, imonth)

        if 'FTB D' in contract:
            d = int(contract[8:10])
            m = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep','Oct', 'Nov', 'Dec'].index(contract[10:13]) + 1
            y = int(contract[14:]) + 2000
            dt = datetime.date(y, m, d)
            return (dt, dt, 'D')

        return None

    #if not self.check_trading_date_on_table('mibel_contracts'):
    #    return False

    if 1==2:
        attempts = 1
        max_attempts = 5
        while attempts <= max_attempts:
            try:
                driver = webdriver.Chrome(self.selenium_driver_path)
                driver.implicitly_wait(20)
                driver.get("https://www.omip.pt/en/dados-mercado?date={DATE}&product=EL&zone=ES&instrument=FTB".format(DATE=str(self.trading_date)))
                html_txt = driver.page_source
                driver.close()
                break
            except:
                self.logger.error('Error accesing OMIP webpage - trying attempt {}'.format(attempts))
                attempts+=1
        if attempts==max_attempts:
            raise WebpageNotAviable

    import requests
    r = requests.get("https://www.omip.pt/en/dados-mercado?date={DATE}&product=EL&zone=ES&instrument=FTB".format(DATE=str(self.trading_date)))
    r.encoding = 'ISO-8859-1'
    html_txt = str(r.content)

    import bs4
    soup = bs4.BeautifulSoup(html_txt, 'html5lib')
    rows = [tr.findAll('td') for tr in soup.findAll('tr')]

    lst_contrats, lst_dates = [], []
    lst_quarters, lst_months = {}, {}
    i = 1

    for r in rows:
        if len(r) == 20:
            mid_contracts_list = []
            flag_valid_contract = False
            for idx, item in enumerate(r):
                if item.contents[0] != 'Contract name':
                    if idx == 0:
                        contract = item.contents[0]
                        tipo = str(contract).split('</div></div></span>')[1]
                        tipo = tipo.split('</strong>')[0]
                        contract_date_parsed = parse_dates(tipo)
                        if contract_date_parsed != None:
                            mid_contracts_list.append(tipo)
                            mid_contracts_list.append(contract_date_parsed[2])
                            mid_contracts_list.append(contract_date_parsed[0])
                            mid_contracts_list.append(contract_date_parsed[1])
                            flag_valid_contract = True

                            if contract_date_parsed[2] == 'Y':
                                lst_quarters[contract_date_parsed[3]] = []

                            if contract_date_parsed[2] == 'Q':
                                if not contract_date_parsed[3] in lst_quarters:
                                    lst_quarters[contract_date_parsed[3]] = []
                                lst_quarters[contract_date_parsed[3]].append(contract_date_parsed[4])
                                if not '{}-{}'.format(contract_date_parsed[3],contract_date_parsed[4]) in lst_months:
                                    lst_months['{}-{}'.format(contract_date_parsed[3],contract_date_parsed[4])] = []

                            if contract_date_parsed[2] == 'M':
                                if not '{}-{}'.format(contract_date_parsed[3],contract_date_parsed[4]) in lst_months:
                                    lst_months['{}-{}'.format(contract_date_parsed[3],contract_date_parsed[4])] = []
                                lst_months['{}-{}'.format(contract_date_parsed[3],contract_date_parsed[4])].append(contract_date_parsed[5])

                    if idx == 14:
                        if flag_valid_contract == True:
                            price = item.contents[0]
                            mid_contracts_list.append(price)

            # Fit dates to ensure continuity
            if len(mid_contracts_list) > 1:
                if len(lst_contrats) > 1:
                    if mid_contracts_list[3] - lst_dates[-1][0] > datetime.timedelta(days=1):
                        lst_contrats[-1][3] = mid_contracts_list[2] - datetime.timedelta(days=1)
                i += 1
                lst_contrats.append(mid_contracts_list)
                lst_dates.append((mid_contracts_list[3], mid_contracts_list[4]))

    db_format_contracts_list = []
    for contract in lst_contrats:
        db_format_contracts_list.append([
            self.trading_date,
            contract[2],
            contract[3],
            contract[1],
            contract[4]
        ])

    contract_years = {}
    l_out = []

    for contract in db_format_contracts_list:
        if contract[3] == 'D':
            l_out.append(contract)
        elif contract[3] == 'W':
            l_out.append(contract)
        elif contract[3] == 'M':
            if not contract[1].year in contract_years:
                contract_years[contract[1].year] = {}
            if not math.ceil(float(contract[1].month) / 3) in contract_years[contract[1].year]:
                contract_years[contract[1].year][math.ceil(float(contract[1].month) / 3)] = {}
            if not contract[1].month in contract_years[contract[1].year][math.ceil(float(contract[1].month) / 3)]:
                contract_years[contract[1].year][math.ceil(float(contract[1].month) / 3)][contract[1].month] = contract
        elif contract[3] == 'Q':
            if not contract[1].year in contract_years:
                contract_years[contract[1].year] = {}
            if not math.ceil(float(contract[1].month) / 3) in contract_years[contract[1].year]:
                contract_years[contract[1].year][math.ceil(float(contract[1].month) / 3)] = {}
            contract_years[contract[1].year]['Q' + str(math.ceil(float(contract[1].month) / 3))] = contract
        elif contract[3] == 'Y':
            if not contract[1].year in contract_years:
                contract_years[contract[1].year] = {}
            contract_years['Y' + str(contract[1].year)] = contract

    for year in lst_quarters:
        if self.trading_date.year != year:
            if lst_quarters[year]:
                if len(lst_quarters[year]) == 4:
                    l_out.append(contract_years[year]['Q1'])
                    l_out.append(contract_years[year]['Q2'])
                    l_out.append(contract_years[year]['Q3'])
                    l_out.append(contract_years[year]['Q4'])
                    contract_years[year]['Q1'] = None
                    contract_years[year]['Q2'] = None
                    contract_years[year]['Q3'] = None
                    contract_years[year]['Q4'] = None
                else:
                    l_out.append(contract_years['Y' + str(year)])
                    contract_years['Y' + str(year)] = None
            else:
                l_out.append(contract_years['Y' + str(year)])
                contract_years['Y' + str(year)] = None
        else:
            for quarter in lst_quarters[year]:
                l_out.append(contract_years[year]['Q{}'.format(quarter)])
                contract_years[year]['Q{}'.format(quarter)] = None

    for quarter in lst_months:
        if '{}-{}'.format(self.trading_date.year, math.ceil(float(self.trading_date.month / 3))) != quarter:
            if lst_months[quarter]:
                if len(lst_months[quarter]) == 3:
                    for month in lst_months[quarter]:
                        l_out.append(contract_years[int(quarter.split('-')[0])][int(quarter.split('-')[1])][month])
                        contract_years[int(quarter.split('-')[0])][int(quarter.split('-')[1])][month] = None
        else:
            for month in lst_months[quarter]:
                l_out.append(contract_years[int(quarter.split('-')[0])][int(quarter.split('-')[1])][month])
                contract_years[int(quarter.split('-')[0])][int(quarter.split('-')[1])][month] = None

    self.mysql.insert_data(table='mibel_contracts', fields=['tradate', 'f_d_day', 'l_d_day', 'type', 'price'], data=l_out)
    return True


def update_forward_spot(self):
    if not self.check_trading_date_on_table('forward_spot'):
        return False
    query = 'call forward_spot_price(%s)'
    cursor = self.mysql.execute_procedure(query=query, data=[self.trading_date])
    self.logger.info('Forward spot curve uploaded')
    return True


def update_yield(self):
    if not self.check_trading_date_on_table('rendimientos'):
        return False
    query = 'call var_rendimientos(%s)'
    cursor = self.mysql.execute_procedure(query=query, data=[self.trading_date])
    self.logger.info('Yield values uploaded')
    return True


def update_var(self):
    if not self.check_trading_date_on_table('var'):
        return False
    query = 'call var_VaR_diario(%s)'
    cursor = self.mysql.execute_procedure(query=query, data=[self.trading_date])
    self.logger.info('Risk values uploaded')
    return True


def check_trading_date_on_table(self,table):
    sql="SELECT MAX(tradate) FROM {};".format(table)
    cursor = self.mysql.execute_procedure(query=sql, data=[])
    max_tradate = cursor.fetchone()
    if max_tradate:
        max_tradate = max_tradate[0]
    if max_tradate:
        if max_tradate >= self.trading_date:
            self.logger.info('Values on {} already uploaded'.format(table))
            return False
    return True

def update_fixings(self):
    self.update_prices()
    self.update_spot()
    self.update_kest()

def update_forward(self):
    self.update_contracts()
    self.update_forward_spot()
    self.update_yield()
    self.update_var()

update_methods = {
    'update_spot': update_spot,
    'update_prices': update_prices,
    'update_kest': update_kest,
    'update_contracts': update_contracts,
    'update_forward_spot': update_forward_spot,
    'update_yield': update_yield,
    'update_var': update_var,
    'update_ht_prices': update_ht_prices,
    'check_trading_date_on_table': check_trading_date_on_table,
    'update_fixings': update_fixings,
    'update_forward': update_forward,
}
