from EsiosWebService import esios_web_functions as web_service
from managers.mysql_manager import MySQL_Manager
import constants as cons
import datetime
from datetime import timedelta, date
import logging.config,logging
import offer_maker, index_maker
from EsiosWebService.data_checker import *
from utils.offer_tools import check_if_index



logging.config.fileConfig("C:/Users/Diego/Documents/PWR_Params_AB/logging.cfg")
logger = logging.getLogger("OliviaOfertMaker")

# Get the actual date
days = [
    # datetime.datetime(year=2022, month=10, day=1),
    # datetime.datetime(year=2023, month=1, day=1),
    # datetime.datetime(year=2023, month=2, day=1),
    datetime.datetime(year=2023, month=7, day=1),
]

mysql = MySQL_Manager(dict_connection_values= cons.dict_values_DB, logger = logger)

for date_today in days:
    print(date_today)
    if date_today.day == 1:
        if date_today.month == 1:
            date_before = datetime.datetime(day=1, month=12, year=date_today.year - 1)
        else:
            date_before = datetime.datetime(day=1, month=date_today.month - 1, year=date_today.year)
    else:
        date_before = date_today - timedelta(days=365)
    logger.info("Loading data " + str(date_today))
    # Calcula los param A y B
    offer_maker.main_runner(date_inicial = date_today, mysql = mysql, date_before=date_before)
    logger.info("Cargados parámetros")
    # Calcula el índice para contratos indexados
    # index_maker.main_runner(date_inicial=date_today, mysql=mysql)

    logger.info("Cargados índices")




