import os
from os.path import basename
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
from email.mime.application import MIMEApplication
import base64

import pickle

from googleapiclient.discovery import build
from google_auth_oauthlib.flow import InstalledAppFlow
from google.auth.transport.requests import Request


# [EMAIL]
EMAIL_SMTP = "pop.gmail.com"
EMAIL_PORT = 995
EMAIL_USER = "info@nrgconsulting.es"
EMAIL_PW = ""

EMAIL_ORIGIN = "info@nrgconsulting.es"
EMAIL_RECEIVER = "info@nrgconsulting.es"
EMAIL_TITLE_ENRG = "tuples_daily {:%Y%m%d} enrg"
EMAIL_TITLE_ETRM = "tuples_daily {:%Y%m%d} etrm"
BCC_FILE = 'bcc.txt'



SCOPES = ['https://www.googleapis.com/auth/gmail.readonly', 'https://www.googleapis.com/auth/gmail.send']

class EmailSender():
    __task_type = "SEND_DATA"

    def _init_connection(self):
        os.chdir(r'C:\Users\NRG ofi\Dropbox\eNRG\tareascapwatt\codvenv\omip_mail')
        creds = None
        if os.path.exists(r"token.pickle"):
            with open(r"token.pickle", 'rb') as token:
                creds = pickle.load(token)
        # If there are no (valid) credentials available, let the user log in.
        if not creds or not creds.valid:
            if creds and creds.expired and creds.refresh_token:
                creds.refresh(Request())
            else:
                flow = InstalledAppFlow.from_client_secrets_file(
                    r"client_secret.json", SCOPES)
                creds = flow.run_local_server(port=0)
            # Save the credentials for the next run
            with open("token.pickle", 'wb') as token:
                pickle.dump(creds, token)

        service = build('gmail', 'v1', credentials=creds)

        # Call the Gmail API
        results = service.users().labels().list(userId='me').execute()
        labels = results.get('labels', [])
        if not labels:
            print("No labels found.")
        else:
            print("Labels:")
            for label in labels:
                print(label['name'])

        return service

    def _generate_MIME_content(self,*, origin, to, subject, body, bcc=None):
        msg = MIMEMultipart()
        msg['From'] = origin
        msg['To'] = ", ".join(to)
        msg['Bcc'] = bcc
        msg['Subject'] = subject
        msg.attach(MIMEText(body, 'plain'))
        return msg

    def _attach_file_to_MIME(self, msg, filepath):
        if filepath and os.path.isfile(filepath):
            
            with open(filepath, "rb") as file:
                attachment = MIMEApplication(file.read(), Name=basename(filepath))
            attachment['Content-Disposition'] = 'attachment; filename="%s"' % basename(filepath)
            msg.attach(attachment)
        else:
            print('no file', filepath)
        return msg

    def _generate_email_data(self, *, to=[], bcc, subject, body, attachment_path=None):
        origin = EMAIL_ORIGIN
        msg = self._generate_MIME_content(origin=origin, to=to, subject=subject, body=body, bcc=bcc)
        for attch_path in attachment_path:
            msg = self._attach_file_to_MIME(msg, attch_path)
        b64_bytes = base64.urlsafe_b64encode(msg.as_bytes())
        b64_string = b64_bytes.decode()
        body = {'raw': b64_string}
        if not isinstance(to,list):
            to = [to]
        return origin, to, body


    def send_email(self, contacts, hidden_contacts, subject_enrg, subject_etrm, body, 
                   attachment_path_enrg, attachment_path_etrm):
        service = self._init_connection() #  smtp =
        to_do = [[subject_enrg, attachment_path_enrg], [subject_etrm, attachment_path_etrm]]
        for x in to_do:
            print('sending', x)
            
            origin, to, content = self._generate_email_data(
                to= contacts,
                bcc= hidden_contacts,  # todo hidden_contacts ?
                subject= x[0],
                body= body,
                attachment_path = x[1]
            )
            
            msg = (service.users().messages().send(userId='me', body=content).execute())

        return 200

