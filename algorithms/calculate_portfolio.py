
import os
import time
import datetime
import re
from multiprocessing import Pool

import numpy as np
import pandas as pd
from sklearn.linear_model import LinearRegression
from sklearn.preprocessing import PolynomialFeatures

import nrg_sf.common_utils.tools as tl
import nrg_sf.common_utils.evaluate as evaluate
from nrg_sf.eNRGPower.constants import *

def portfolio_calculation(self, *, portfolios=[], trading_dates=[], formulae_calculation=False, sensitivity_calculation=False, standard_mode=False):
    self.logger.info('Commodity {} portfolios process calculation'.format(self.slug))
    
    #
    # COMMON DATA FOR ALL PORTFOLIOS
    #

    # Min & Max delivery date from all portfolios
    portfolios_list = ''
    for pf, nxt in tl.lookahead(portfolios):
        portfolios_list += str(pf)
        if nxt == True:
            portfolios_list += ','

    sql = 'SELECT MIN(inicio_consumo), MAX(fin_consumo) FROM portfolio WHERE idportfolio IN ({})'.format(portfolios_list)
    cursor = self.mysql.execute_procedure(query=sql, data=[])
    cursor = cursor.fetchone()
    min_delivery_date, max_delivery_date = cursor[0], cursor[1]

    # Fixed prices
    if formulae_calculation:
        self.logger.info('Formulae fixing prices importing process')
        self.spot_prices = {}
        for trading_date in trading_dates:
            self.spot_prices[trading_date] = self.__import_fixed_values_by_trading_date(
                min_delivery_date=min_delivery_date,
                max_delivery_date=max_delivery_date,
                trading_date=trading_date
            )
        self.other_markets_prices, self.structural_prices = self.__import_fixed_values_by_fixing_date(
                min_delivery_date=min_delivery_date,
                max_delivery_date=max_delivery_date
            )

    # Sensitivity for last trading day
    if sensitivity_calculation:
        self.logger.info('Sensitiviy calculation process')
        for trading_date in reversed(trading_dates):
            # Check for aviability of trading date
            sql = 'SELECT tradate FROM mibel_contracts WHERE tradate = %s GROUP BY tradate;'
            cursor = self.mysql.execute_procedure(query=sql, data=[trading_date])
            current_trading_date = cursor.fetchone()
            if current_trading_date:
                current_trading_date = current_trading_date[0]
                prices_vector = self.__prices_vector(
                    min_delivery_date=min_delivery_date,
                    max_delivery_date=max_delivery_date,
                    trading_date=current_trading_date
                )
                self.log_time('debug', 'sensitivity data')
                break

    #
    # ITER OVER SINGLE PORTFOLIO
    #
    list_to_return = []
    for portfolio in portfolios:
        # Porfolio data
        sql = '''SELECT ruta,lmd,ltd,lcc,cliente.idcliente,inicio_consumo,fin_consumo,formula,historic_filepath,single_result_pattern,email,undervar_hierarchy FROM portfolio 
                 JOIN cliente ON portfolio.idcliente = cliente.idcliente WHERE idportfolio = %s;'''
        cursor = self.mysql.execute_procedure(
            query=sql,
            data=[portfolio]
        )
        data = cursor.fetchone()
        xls_data = {
            'xls_path': data[0],
            'lmd': data[1],
            'ltd': data[2],
            'last_column': data[3],
        }
        deal_data = {
            'id_client': data[4],
            'start_delivery_date': data[5],
            'end_delivery_date': data[6],
            'total_cost_formula': data[7],
            'undervar_hierarchy': data[11]
        }
        csv_data = {
            'historic_filepath': data[8],
            'single_result_pattern': data[9]
        }
        client_data = {
            'email': data[10]
        }
        sql = 'SELECT `signal`, `change`, `open_signal` FROM presupuestos WHERE idportfolio = %s ORDER BY idpresupuesto DESC LIMIT 1;'
        cursor = self.mysql.execute_procedure(
            query=sql,
            data=[portfolio]
        )
        data = cursor.fetchone()
        deal_data['signal'] = data[0]
        deal_data['change'] = data[1]
        deal_data['open_signal'] = data[2]

        result, daily_output, last_month_output = self.portfolio_output_data(
            id_portfolio=portfolio,
            trading_dates=trading_dates,
            formulae_calculation=formulae_calculation,
            sensitivity_calculation=sensitivity_calculation,
            prices_vector=prices_vector if sensitivity_calculation else None,
            formula=deal_data['total_cost_formula']
        )
        if result:
            last_daily_output = daily_output.tail(1)
            if standard_mode:
                self.standard_validations(
                    portfolio=portfolio,
                    last_daily_output=last_daily_output,
                    deal_data=deal_data,
                    client_data=client_data
                )
                self.standard_export(
                    portfolio=portfolio,
                    daily_output=daily_output,
                    monthly_output=last_month_output,
                    xls_data=xls_data,
                    csv_data=csv_data,
                    trading_date=trading_dates[-1]
                )
            list_to_return.append([
                portfolio,
                daily_output, 
                last_month_output,
                deal_data,
                xls_data,
                csv_data
            ])
        else:
            list_to_return.append(None)

    return list_to_return

def portfolio_output_data(self,*, id_portfolio, trading_dates=[], formulae_calculation=False, sensitivity_calculation=False, prices_vector=None, formula=None):
    self.logger.info('Portfolio {} calculation'.format(id_portfolio))
    
    daily_output_dfs = []
    last_calculated_flag = False
    last_calculated_trading_date = None
    first_calculated_trading_date = None

    if formulae_calculation:
        deal_fare_curves = {}
        sql = 'SELECT tarifa, iddeal_key FROM deal_keys WHERE idportfolio = %s AND tipo = 2'
        curves_cursor = self.mysql.execute_procedure(query=sql, data=[id_portfolio])
        for result in curves_cursor:
            fare = result[0]
            deal_key = result[1]
            sql = 'SELECT fdate, MONTH(fdate) AS month, YEAR(fdate) AS year, fhour, `value` FROM deal_fisico_tarifa WHERE deal_key = %s'
            deal_fare_curves[fare] = self.mysql.execute_procedure(query=sql, data=[deal_key], to_dataframe=True)

    for trading_date, not_last, idx in tl.lookahead_enumerate(trading_dates):
        self.logger.debug('Trading date {} calculation'.format(trading_date))
        # Check for aviability of trading date
        sql='SELECT COUNT(*), tradate FROM forward_spot WHERE tradate=%s GROUP BY tradate;'
        cursor = self.mysql.execute_procedure(query=sql, data=[trading_date])
        forward_spot_count = cursor.fetchone()
        if forward_spot_count:
            # Fixed prices
            current_trading_date=forward_spot_count[1]
            last_calculated_trading_date=current_trading_date
            if not first_calculated_trading_date:
                first_calculated_trading_date=current_trading_date
            sql='call `&informe_0_portfolio_test`(%s, %s);'
            df = self.mysql.execute_procedure(
                query=sql,
                data=[current_trading_date, id_portfolio],
                to_dataframe=True
            )
            if formulae_calculation:
                total_formulae_cost = 0
                for fare_code in deal_fare_curves:
                    fare_price, fare_price_by_month = self.__formulae_calculation(
                        fare_code=fare_code,
                        deal_curve=deal_fare_curves[fare_code],
                        spot_prices=self.spot_prices[trading_date],
                        other_markets_prices=self.other_markets_prices,
                        structural_prices=self.structural_prices,
                        formula=formula
                    )
                    total_formulae_cost+=fare_price
                total_cost_df = pd.DataFrame([total_formulae_cost], columns=['TotalCost'])
                df = pd.concat([df, total_cost_df], axis=1)
            daily_output_dfs.append(df)
            purchase_price=df.PrecioFinal.sum()
            if not not_last:
                last_calculated_flag=True
                sql='call `&informe_1_portfolio_test`(%s, %s);'
                monthly_output_df = self.mysql.execute_procedure(
                    query=sql,
                    data=[current_trading_date, id_portfolio],
                    to_dataframe=True
                )
        else:
            self.logger.warning('No trading date')

    if not last_calculated_flag:
        sql='call `&informe_1_portfolio_test`(%s, %s);'
        monthly_output_df=self.mysql.execute_procedure(
            query=sql,
            data=[last_calculated_trading_date, id_portfolio],
            to_dataframe=True
        )

    if sensitivity_calculation:
        thermal_gap_dict = {}
        thermal_gap_pd = {}
        # Dict for RG output
        #  Stage 1: Sensitivity Price Range
        #  Stage 2: Monthly aggregation M-YYYY ( OpenEnergy * Sensitivity Price Range )
        open_energy_cursor = self.mysql.execute_procedure(
            query='CALL `energia_abierta_portfolio_test`(%s,%s)',
            data=[last_calculated_trading_date, id_portfolio]
        )
        for open_energy_row in open_energy_cursor:
            month_key = '{}-{}'.format(str(open_energy_row[4]), str(open_energy_row[5]))
            day_key = '{}-{}'.format(str(open_energy_row[1]), str(open_energy_row[2]))
            for price_idx in range(6):
                if not price_idx in thermal_gap_dict:
                    thermal_gap_dict[price_idx] = {}
                if month_key in thermal_gap_dict[price_idx]:
                    thermal_gap_dict[price_idx][month_key] += float(open_energy_row[3]) * prices_vector[day_key][price_idx]
                else:
                    thermal_gap_dict[price_idx][month_key] = float(open_energy_row[3]) * prices_vector[day_key][price_idx]

        ht_array = []
        for price_idx in range(6):
            ht_row=[]
            for month_key in thermal_gap_dict[price_idx]:
                ht_row.append(thermal_gap_dict[price_idx][month_key])
            ht_array.append(ht_row)
        df = pd.DataFrame(tl.traspose_list(ht_array), columns=['Sd3', 'Sd2', 'Sd1', 'Sm1', 'Sm2', 'Sm3'])
        monthly_output_df = pd.concat([monthly_output_df, df], axis=1)

        # Sensitivity Signals
        # Month Signal
        r_senb = {}
        sensitivity_base = []
        monthly_sensitivity_row = []
        for idx, month in monthly_output_df.iterrows():
            sensitivity_base.append(month.Sm1 - month.Sd1)

        r_senb[0] = sum(sensitivity_base) / len(sensitivity_base)
        r_senb[3] = max(sensitivity_base)
        r_senb[-3] = min(sensitivity_base)

        r_senb[1] = ((r_senb[3] - r_senb[0]) / 3) + r_senb[0]
        r_senb[2] = (((r_senb[3] - r_senb[0]) / 3) * 2) + r_senb[0]

        r_senb[-1] = r_senb[0] + ((r_senb[-3] - r_senb[0]) / 3)
        r_senb[-2] = r_senb[0] + (((r_senb[-3] - r_senb[0]) / 3) * 2)

        for idx, month in monthly_output_df.iterrows():
            if r_senb[-1] <= sensitivity_base[idx] <= r_senb[1]:
                monthly_sensitivity_row.append(0)
                continue
            elif r_senb[-2] <= sensitivity_base[idx] <= r_senb[2]:
                if sensitivity_base[idx] > r_senb[0]:
                    monthly_sensitivity_row.append(1)
                else:
                    monthly_sensitivity_row.append(-1)
                continue
            else:
                if sensitivity_base[idx] > r_senb[0]:
                    monthly_sensitivity_row.append(2)
                else:
                    monthly_sensitivity_row.append(-2)

        # Quarter Signal
        r_senb = {}
        sensitivity_base = []
        quarterly_sensitivity_row = []
        # for bound in [(0,2),(3,5),(6,8),(9,11)]:
        for bound in [(i*3, (i*3)+2) for i in range(int(monthly_output_df.shape[0]/3))]:
            mdf = monthly_output_df[bound[0]:bound[1]]
            sensitivity_base.append(mdf.Sm1.sum() - mdf.Sd1.sum())

        r_senb[0] = sum(sensitivity_base) / len(sensitivity_base)
        r_senb[3] = max(sensitivity_base)
        r_senb[-3] = min(sensitivity_base)

        r_senb[1] = ((r_senb[3] - r_senb[0]) / 3) + r_senb[0]
        r_senb[2] = (((r_senb[3] - r_senb[0]) / 3) * 2) + r_senb[0]

        r_senb[-1] = r_senb[0] + ((r_senb[-3] - r_senb[0]) / 3)
        r_senb[-2] = r_senb[0] + (((r_senb[-3] - r_senb[0]) / 3) * 2)
        
        for idx, month in monthly_output_df.iterrows():
            indx = int(idx / 3)
            
            if r_senb[-1] <= sensitivity_base[indx] <= r_senb[1]:
                quarterly_sensitivity_row.append(0)
                continue
            elif r_senb[-2] <= sensitivity_base[indx] <= r_senb[2]:
                if sensitivity_base[indx] > r_senb[0]:
                    quarterly_sensitivity_row.append(1)
                else:
                    quarterly_sensitivity_row.append(-1)
                continue
            else:
                if sensitivity_base[indx] > r_senb[0]:
                    quarterly_sensitivity_row.append(2)
                else:
                    quarterly_sensitivity_row.append(-2)

        sensitivity = [monthly_sensitivity_row, quarterly_sensitivity_row]
        df = pd.DataFrame(tl.traspose_list(sensitivity), columns=['MonthSignal', 'QuarterSignal'])
        monthly_output_df = pd.concat([monthly_output_df, df], axis=1)

    # Conact all trading dates with trading dates with market values
    daily_output_dfs = pd.concat(daily_output_dfs, axis=0)
    total_trading_dates_df = pd.DataFrame(trading_dates, columns=['Tradate'])
    daily_output_dfs = total_trading_dates_df.set_index('Tradate').join(daily_output_dfs.set_index('Tradate'))
    daily_output_dfs.fillna(False, inplace=True)
    daily_output_dfs.reset_index(level=0, inplace=True)

    # Fill empty rows
    previous_idx = None
    for idx, month in daily_output_dfs.iterrows():
        if not month.CostePnL:
            if previous_idx:
                current_tradate = month.Tradate
                daily_output_dfs.iloc[idx] =  daily_output_dfs.iloc[previous_idx]
                daily_output_dfs.iloc[idx, daily_output_dfs.columns.get_loc('Tradate')] = current_tradate
        else:
            previous_idx = idx

    daily_output_dfs = daily_output_dfs.drop(daily_output_dfs[daily_output_dfs.CostePnL == False].index)

    return True, daily_output_dfs, monthly_output_df

def __thermal_gap_regressions(self, trading_date):
    regresiones_ht_fdep, regresiones_ranges = {}, {}
    exponent = 3
    for period in range(1, 10):
        x, y = [], []
        cur = self.mysql.execute_procedure(
            query='call ht_listado(%s, %s);',
            data=[trading_date, period]
        )
        for result in cur:
            x.append(result[2])
            y.append(result[3])

        middle_point = float(min(x) + max(x) / 2)

        ranges = [
            middle_point * float(-0.3),
            middle_point * float(-0.2),
            middle_point * float(-0.1),
            middle_point * float(0.1),
            middle_point * float(0.2),
            middle_point * float(0.3)
        ]

        X = np.reshape(y, (-1, 1))
        Y = np.reshape(x, (1, -1))
        Y = np.squeeze(Y)
        X = X.astype(float)
        Y = Y.astype(float)
        lin_reg = LinearRegression()
        lin_reg.fit(X, Y)
        poly_reg = PolynomialFeatures(degree=exponent)
        X_poly = poly_reg.fit_transform(X)
        poly_reg.fit(X_poly, Y)
        lin_reg_2 = LinearRegression()
        lin_reg_2.fit(X_poly, Y)

        coefficients = []
        i = float(lin_reg_2.intercept_)
        a = float(lin_reg_2.coef_[1])
        b = float(lin_reg_2.coef_[2])
        c = float(lin_reg_2.coef_[3])
        coefficients = [i, a, b, c]
        regresiones_ht_fdep[period] = coefficients
        regresiones_ranges[period] = ranges

    return regresiones_ht_fdep, regresiones_ranges

def __prices_vector(self,*, min_delivery_date, max_delivery_date, trading_date):

    coef = self.__thermal_gap_regressions(trading_date)
    sql='call listado_omip_periodos(%s, %s, %s);'
    cursor = self.mysql.execute_procedure(query=sql, data=[min_delivery_date, max_delivery_date, trading_date])
    prices_vector = {}
    for result in cursor:
        prices = []
        current_price = float(result[2])
        current_period = float(result[3])
        ground_thermal_gap = coef[0][current_period][0] + \
                             (coef[0][current_period][1] * current_price) + (
                             coef[0][current_period][2] * (current_price ** 2)) + (
                             coef[0][current_period][3] * (current_price ** 3))
        for reference in coef[1][current_period]:
            current_thermal_gap = ground_thermal_gap
            current_price = float(result[2])
            while 1:
                if reference > 0:
                    incremental_price = 0.01
                else:
                    incremental_price = -0.01
                current_price += incremental_price
                comparison_thermal_gap = \
                    coef[0][current_period][0] + \
                    (coef[0][current_period][1] * current_price) + (
                    coef[0][current_period][2] * (current_price ** 2)) + (
                    coef[0][current_period][3] * (current_price ** 3))
                if abs(comparison_thermal_gap - ground_thermal_gap) >= abs(reference):
                    prices.append(current_price)
                    break
                if reference > 0:
                    if current_thermal_gap > comparison_thermal_gap :
                        prices.append(current_price)
                        break
                else:
                    if current_thermal_gap < comparison_thermal_gap :
                        prices.append(current_price)
                        break
                current_thermal_gap = comparison_thermal_gap
        fdate = str(result[0])
        fhour = str(result[1])
        prices_vector['{}-{}'.format(fdate, fhour)] = prices

    return prices_vector

def __import_fixed_values_by_trading_date(self,*, min_delivery_date, max_delivery_date, trading_date):

    spot_prices = {}
    sql = '''SELECT fdate, fhour, `value` FROM forward_spot 
             WHERE fdate >= '{MIN_DATE}' AND fdate <= '{MAX_DATE}' AND tradate = '{TRA_DATE}' 
             ORDER BY fdate,fhour ASC'''.format(MIN_DATE=min_delivery_date, MAX_DATE=max_delivery_date, TRA_DATE=trading_date)
    cur = self.mysql.execute_procedure(query=sql, data=[])
    for result in cur:
        if not result[0] in spot_prices.keys():
            spot_prices[result[0]]={}
        spot_prices[result[0]][result[1]]=result[2]

    return spot_prices

def __import_fixed_values_by_fixing_date(self,*, min_delivery_date, max_delivery_date):

    other_markets_prices = {}
    sql = '''SELECT MONTH(edate), esiosid, AVG(`value`) FROM fixing_prices
             WHERE edate >= '{MIN_DATE}' AND edate <= '{MAX_DATE}' AND
             esiosid IN (793, 794, 796, 797, 798, 799, 800, 802, 803, 999, 1276, 1285, 1366) 
             GROUP BY esiosid, MONTH(edate), YEAR(edate);'''.format(MIN_DATE=min_delivery_date - datetime.timedelta(days=365), MAX_DATE=min_delivery_date)
    prices_cursor = self.mysql.execute_procedure(query=sql, data=[])

    for result in prices_cursor:
        if not result[0] in other_markets_prices.keys():
            other_markets_prices[result[0]]={}
        other_markets_prices[result[0]][result[1]] = result[2]

    structural_prices = {}
    sql = '''SELECT tarifa,psdate,pshour,pagoscapacidad,coefperdidas,atrenergia FROM paramstruct 
             WHERE psdate >= '{MIN_DATE}' AND psdate <= '{MAX_DATE}' AND tarifa < 99 
             ORDER BY psdate,pshour ASC'''.format(MIN_DATE=min_delivery_date, MAX_DATE=max_delivery_date)
    cur = self.mysql.execute_procedure(query=sql, data=[])

    for result in cur:
        fare,date,hour = result[0],result[1],result[2]
        PPC,PERD,ATR = result[3],result[4],result[5]
        if not fare in  structural_prices.keys():
            structural_prices[fare] = {}
        if not date in structural_prices[fare].keys():
            structural_prices[fare][date] = {}
        if not hour in structural_prices[fare][date].keys():
            structural_prices[fare][date][hour] = {}
        structural_prices[fare][date][hour]['PPC']=PPC
        structural_prices[fare][date][hour]['PERD']=PERD
        structural_prices[fare][date][hour]['ATR']=ATR

    return other_markets_prices, structural_prices

def __formulae_calculation(self,*, fare_code, deal_curve, spot_prices, other_markets_prices, structural_prices, formula):
    spot_matchs = re.findall('s[0-9,A-Z]*', formula)
    prices_matchs = re.findall('r[0-9,A-Z]*', formula)
    param_matchs = re.findall('p[0-9,A-Z]*', formula)

    total_cost_by_month = {}
    total_cost = 0

    for idx, curve_row in deal_curve.iterrows():
        current_formula = formula.replace('_&_', '')
        for spot in spot_matchs:
            new_price = spot_prices[curve_row.fdate][curve_row.fhour]
            if new_price:
                current_formula = current_formula.replace(spot, str(round(new_price, 4)))
            else:
                current_formula = current_formula.replace(spot, str(0))
        for price in prices_matchs:
            new_price = other_markets_prices[curve_row.month][int(price[1:])]
            if new_price:
                current_formula = current_formula.replace(price, str(round(new_price, 4)))
            else:
                current_formula = current_formula.replace(price, str(0))
        for param in param_matchs:
            new_price = structural_prices[fare_code][curve_row.fdate][curve_row.fhour][param[1:]]
            if new_price:
                current_formula = current_formula.replace(param, str(round(new_price, 4)))
            else:
                current_formula = current_formula.replace(param, str(0))

        current_formula = current_formula.replace('ENE', str(curve_row.value))

        calculated = evaluate.evaluarExpresion(current_formula)
        total_cost += calculated

        if not curve_row.month in total_cost_by_month.keys():
            total_cost_by_month[curve_row.month]=0
        total_cost_by_month[curve_row.month]+=calculated

    return total_cost, total_cost_by_month

def standard_validations(self,*, portfolio, last_daily_output, deal_data, client_data, send_mail=False):

    var_evaluation = last_daily_output.CosteVaR.sum() / last_daily_output.Presupuesto_Maximo.sum()
    pmax_evaluation = False if (last_daily_output.Presupuesto_Maximo.sum() * (1 - deal_data['signal'])) > last_daily_output.CosteVaR.sum() else True
    
    has_closed_position = True if last_daily_output.Energia_Cerrada.sum() > 0 else False
    action_decrease_bugdet = False
    action_open_position = False

    self.logger.debug('Portfolio ID {} - underbudget signal set up at {}% lower'.format(portfolio, round((1 - deal_data['signal']) * 100, 2)))
    self.logger.debug('Portfolio ID {} - evaluating VaR for {}% value of (VaR / MaxBudget)'.format(portfolio, round(var_evaluation * 100, 2)))   

    if var_evaluation > 1:
        # VaR over PMax
        self.logger.warning('VaR over PMax for Portfolio {}'.format(portfolio))
        if send_mail:
            PMAX = round(float(last_daily_output.Presupuesto_Maximo) / 1000000, 2)
            VAR = round(float(last_daily_output.CosteVaR) / 1000000, 2)
            with open(self.over_cost_msg) as over_cost_msg:
                html_text = over_cost_msg.read().replace('\n', '')
                html_text = html_text.format(
                    PORTFOLIO=portfolio,
                    PMAX=str(PMAX),
                    VAR=str(VAR),
                    OVERCOST=str(PMAX-VAR),
                    OVERCOST_PERCENT=str(round((var_evaluation-1)*100,2)),
                )
                subject = self.over_cost_subject.format(PORTFOLIO=portfolio)
                self.email.send_email(
                    email_msg=html_text,
                    subject=subject,
                    reciver=client_data['email']
                )
    elif pmax_evaluation:
        # VaR in range
        self.logger.debug('VaR in acceptable range')
    else:
        if not has_closed_position:
            action_decrease_bugdet = True
        else:
            if deal_data['signal'] == deal_data['open_signal']:
                if deal_data['undervar_hierarchy'] == 'action_decrease_bugdet':
                    action_decrease_bugdet = True
                elif deal_data['undervar_hierarchy'] == 'action_open_position':
                    action_open_position = True
            else:
                if var_evaluation < deal_data['signal']:
                    action_decrease_bugdet = True
                else:
                    action_open_position = True

    if action_decrease_bugdet:
        # VaR lower PMax
        self.logger.warning('VaR under PMax for Portfolio {}'.format(portfolio))

        sql = "SELECT idpresupuesto FROM presupuestos WHERE idportfolio = %s ORDER BY idpresupuesto DESC LIMIT 1;"
        cursor = self.mysql.execute_procedure(
            query=sql,
            data=[portfolio]
        )
        id_to_update = cursor.fetchone()[0]
        sql = "UPDATE presupuestos SET fecha_final= %s WHERE idpresupuesto = %s;"
        cursor = self.mysql.execute_procedure(
            query=sql,
            data=[last_daily_output.Tradate.values[0], id_to_update]
        )
        new_bugdet = [
            last_daily_output.Tradate.values[0] + ONE_DAY,
            datetime.date(2050, 12, 31),
            2,
            portfolio,
            last_daily_output.Presupuesto_Referencia.values[0],
            last_daily_output.Presupuesto_Maximo.values[0] * (1 - deal_data['change']),
            deal_data['signal'],
            deal_data['change'],
            deal_data['open_signal']
        ]
        fields = ['fecha_inicial', 'fecha_final', 'tipo', 'idportfolio', 'ref', 'max', 'signal', 'change', 'open_signal']
        self.mysql.insert_data(
            table='presupuestos',
            fields=fields,
            data=[new_bugdet]
        )
        if send_mail:
            PMAX = round(float(last_daily_output.Presupuesto_Maximo.sum()) / 1000000, 2)
            VAR = round(float(last_daily_output.CosteVaR.sum()) / 1000000, 2)
            with open(self.under_cost_msg) as under_cost_msg:
                html_text = under_cost_msg.read().replace('\n', '')
                html_text = html_text.format(
                    PORTFOLIO=portfolio,
                    PMAX=str(PMAX),
                    VAR=str(VAR),
                    SIGNAL=str(round((deal_data['signal']) * 100, 2)),
                    CHANGE=str(round((deal_data['change']) * 100, 2)),
                    NEW_BUGDET=str(PMAX * (1 - deal_data['change']))
                )
                subject = self.under_cost_subject.format(PORTFOLIO=portfolio)
                self.email.send_email(
                    email_msg=html_text,
                    subject=subject,
                    reciver=client_data['email']
                )

    if action_open_position:
        # VaR lower PMax
        self.logger.warning('VaR under PMax for Portfolio {}'.format(portfolio))
        if send_mail:
            PMAX = round(float(last_daily_output.Presupuesto_Maximo.sum()) / 1000000, 2)
            VAR = round(float(last_daily_output.CosteVaR.sum()) / 1000000, 2)
            with open(self.open_position_msg) as open_position_msg:
                html_text = open_position_msg.read().replace('\n', '')
                html_text = html_text.format(
                    PORTFOLIO=portfolio,
                    PMAX=str(PMAX),
                    VAR=str(VAR),
                    SIGNAL=str(round((deal_data['signal']) * 100, 2)),
                    CHANGE=str(round((deal_data['change']) * 100, 2)),
                    NEW_BUGDET=str(PMAX * (1 - deal_data['change']))
                )
                subject = self.open_position_subject.format(PORTFOLIO=portfolio)
                self.email.send_email(
                    email_msg=html_text,
                    subject=subject,
                    reciver=client_data['email']
                )

def standard_export(self,*, portfolio, daily_output, monthly_output, xls_data, csv_data, trading_date=None):
    
    # Export single files
    csv_1_filename = '{}{}'.format(self.output_folder, csv_data['single_result_pattern'].format(
            IDP='{num:04d}'.format(num=portfolio),
            TRADATE=datetime.datetime.strftime(trading_date, '%Y%m%d'),
            REPORT=1,
            COMMODITY='eNRGpower'
        )
    )
    csv_2_filename = '{}{}'.format(self.output_folder, csv_data['single_result_pattern'].format(
            IDP='{num:04d}'.format(num=portfolio),
            TRADATE=datetime.datetime.strftime(trading_date, '%Y%m%d'),
            REPORT=2,
            COMMODITY='eNRGpower'
        )
    )
    self.persistence.export_dataframe_to_csv(
        csv_filename=csv_1_filename,
        dataframe=daily_output,
        delimiter=';',
        extends=False,
        header=False,
        transpose=False,
        index=False
    )
    self.persistence.export_dataframe_to_csv(
        csv_filename=csv_2_filename,
        dataframe=monthly_output,
        delimiter=';',
        extends=False,
        header=False,
        transpose=False,
        index=False
    )
    # Increase historic CSV file
    if 1==2:
        self.persistence.export_dataframe_to_csv(
            csv_filename=csv_data['historic_filepath'],
            dataframe=daily_output,
            delimiter=';',
            extends=True,
            header=False,
            transpose=False,
            index=False
        )
    # Increase historic XLS file

def example_outputs(self,*, portfolio, daily_output, monthly_output, xls_data, csv_data, trading_date=None):
    self.persistence.export_dataframe_to_excel(
        excel_filename='D:/Test/foo.xlsx',
        new_file=False,
        dataframe=daily_output,
        sheet='Sheet1',
        transpose=False,
        starting_row=0,
        starting_column=0,
        header=True,
        index=True
    )
    self.persistence.export_dataframe_to_excel(
        excel_filename='D:/Test/foo2.xlsx',
        new_file=True,
        dataframe=daily_output,
        sheet='Sheet1',
        transpose=False,
        starting_row=0,
        starting_column=0,
        header=True,
        index=True
    )
    self.persistence.export_dataframe_to_csv(
        csv_filename='D:/Test/foo.csv',
        dataframe=monthly_output,
        delimiter=',',
        extends=False,
        header=True,
        transpose=False,
        index=True
    )
    self.persistence.export_dataframe_to_csv(
        csv_filename='D:/Test/foo.csv',
        dataframe=monthly_output,
        delimiter=',',
        extends=True,
        header=False,
        transpose=False,
        index=True
    )

update_methods = {
    'portfolio_calculation': portfolio_calculation,
    'portfolio_output_data': portfolio_output_data,
    '__thermal_gap_regressions': __thermal_gap_regressions,
    '__prices_vector': __prices_vector,
    '__import_fixed_values_by_trading_date': __import_fixed_values_by_trading_date,
    '__import_fixed_values_by_fixing_date': __import_fixed_values_by_fixing_date,
    '__formulae_calculation': __formulae_calculation,
    'standard_validations': standard_validations,
    'standard_export': standard_export
}