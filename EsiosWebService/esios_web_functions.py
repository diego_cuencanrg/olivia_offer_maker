import json
import datetime
from calendar import monthrange
import xlrd
import requests

from EsiosWebService.data_checker import *
from managers.calendar_change import change_list, detect_calendar_change


# Raw download functions
def esios_data(start_date, end_date=None, list_ids=[], as_dict=False):

    print(requests.get('https://www.howsmyssl.com/a/check', verify=False).json()['tls_version'])

    if not start_date:
        return False
    else:
        if isinstance(start_date, datetime.datetime) or isinstance(start_date, datetime.date):
            query_startDate = start_date
        else:
            query_startDate = datetime.datetime.strptime(start_date, "%Y-%m-%d")

    if not end_date:
        query_endDate = start_date + datetime.timedelta(days=1) + datetime.timedelta(hours=24)
    else:
        if isinstance(end_date, datetime.datetime) or isinstance(end_date, datetime.date):
            query_endDate = end_date
        else:
            query_endDate = datetime.datetime.strptime(end_date, "%Y-%m-%d") + datetime.timedelta(hours=23)

    base_url = "https://api.esios.ree.es/indicators/{}"
    header_Accept = "application/json; application/vnd.esios-api-v1+json"
    header_Content = "application/json"
    header_Host = "api.esios.ree.es"
    # header_Authorization = "Token token=70abae7293309c384d6ae28f5af0ff02c77a7ef3997cdb392d7adbf135e63ceb"
    header_Authorization = "70abae7293309c384d6ae28f5af0ff02c77a7ef3997cdb392d7adbf135e63ceb"
    query_timeagg = "avg"
    query_timetrunc = "hour"
    query_geoagg = "sum"

    # API e-sios parameters to dictionary (II)
    hHeaders = {
        'Accept': header_Accept,
        'Content-Type': header_Content,
        'Host': header_Host,
        # 'Authorization': header_Authorization
        'x-api-key': header_Authorization
    }
    pParams = {
        'start_date': query_startDate,
        'end_date': query_endDate,
        'time_agg': query_timeagg,
        'time_trunc': query_timetrunc,
        'geo_agg': query_geoagg
    }

    # Getting data from e-sios
    v = []
    d = {}

    for row in list_ids:
        print(row)
        # Download Esios
        # logger.debug('Download ID: ' + str(row))
        r = requests.get(base_url.format(row), headers=hHeaders, params=pParams)
        JSONtext = str(r.content)
        JSONLen = len(JSONtext) - 1
        JSONtext = format_text_esios(JSONtext)
        JSONData = json.loads(JSONtext)
        # JSONData = json.loads(JSONtext[2:JSONLen].replace("\\", "i"))
        for x in JSONData["indicator"]["values"]:
            init_fecha = str(x["datetime"])
            qfecha = init_fecha[0:10]
            qhora = int(init_fecha[11:13]) + 1
            qvalor = x["value"]
            if not as_dict:
                # Export as list
                try:
                    pais = x["geo_name"]
                except KeyError:
                    v.append( [qfecha, qhora, qvalor, str(row)] )
                else:
                    if pais[0:3] == 'Esp':
                        v.append( [qfecha, qhora, qvalor, str(row)] )
            else:
                # Export as dict
                try:
                    pais = x["geo_name"]
                except KeyError:
                    tuple = [str(row), qfecha, qhora, qvalor]
                    if qfecha not in v:
                        v.append(qfecha)
                    if qfecha in d.keys():
                        d[qfecha].append(tuple)
                    else:
                        d[qfecha] = []
                        d[qfecha].append(tuple)
                else:
                    if pais[0:3] == 'Esp':
                        tuple = ( str(row), qfecha, qhora, qvalor )
                        if qfecha not in v:
                            v.append(qfecha)
                        if qfecha in d.keys():
                            d[qfecha].append(tuple)
                        else:
                            d[qfecha] = []
                            d[qfecha].append(tuple)
    if not as_dict:
        return v
    else:
        return v, d

def format_text_esios(text):
    new_text = text.replace('\\xc3\\xa1','á')
    new_text = new_text.replace('\\xc3\\xa9','é')
    new_text = new_text.replace('\\xc3\\xad','í')
    new_text = new_text.replace('\\xc3\\xb3','ó')
    new_text = new_text.replace('\\xc3\\xb1','ñ')
    new_text = new_text.replace('\\n','')
    text_len = len(new_text) - 1
    new_text = new_text[2:text_len]
    # json.loads(new_text)
    return new_text

def kest_data(start_date, download_folder):
    base_url = 'https://api.esios.ree.es/archives/40/download?date='
    strfecha = str(start_date).split(" ")[0]
    fecha = strfecha.split("-")[0] + "-" + strfecha.split("-")[1] + "-"
    idx_dia = start_date.day
    url = base_url + strfecha
    resp = requests.get(url)
    file = download_folder + '/kest_' + strfecha + '.xls'
    output = open(file, 'wb')
    output.write(resp.content)
    output.close()
    xl_workbook = xlrd.open_workbook(file)
    worksheet = xl_workbook.sheet_by_index(0)
    l_kest = []
    for y in range(1,monthrange(start_date.year, start_date.month)[1]+1):
        for x in range(4, 28):
            c = ((str(worksheet.cell(4 + y, x)).replace('number:', '')).replace('text:', '')).replace("'", "")
            fecha2 = fecha + (str(y) if y > 10 else ("0" + str(y)))
            try:
                l_kest.append((fecha2,(x-3), float(c), 999))
            except:
                l_kest.append((fecha2,(x-3), float(c), 999))
    return l_kest

# Update to database functions

def fixing_kest(mysql, start_date, download_folder, logger, on_update=False):
    l_kest = kest_data(start_date=start_date, download_folder=download_folder)
    dt_kest = pd.DataFrame(l_kest, columns = ['Date', 'Hour', 'Value', 'Param'])

    if (backup_saver(dt = dt_kest, fecha = start_date, fecha_fin = start_date, list_ids = ["999"], logger = logger, not_kest=False)):
        return False
    else:
        mysql.insert_data(table='fixing_prices', fields=['edate', 'ehour', 'value', 'esiosid'], data=l_kest,on_update=on_update)
        logger.info("Cargados Kest")
        return True

def fixing_esios(mysql, logger, start_date, end_date=None, list_ids=[], as_dict=None, on_update=False):
    end_date = end_date + timedelta(days=1)
    logger.info('Iniciada carga de ESIOS')
    logger.debug('eSIOS fixing download')
    data_list = esios_data(start_date=start_date, end_date=end_date, list_ids=list_ids, as_dict=False)

    # data_list = change_list(detect_calendar_change(start_date, end_date), data_list)

    dt_upload = pd.DataFrame(data_list, columns = ['Date', 'Hour', 'Value', 'Param'])
    #
    # if(backup_saver(dt = dt_upload, fecha = start_date, fecha_fin = end_date, list_ids = list_ids, logger = logger )):
    #     return False
    # else:
    #     mysql.insert_data(table='fixing_prices', fields=['edate', 'ehour', 'value', 'esiosid'], data=data_list, on_update=on_update)
    #     logger.info('Cargada info de ESIOS')
    #     return True
    mysql.insert_data(table='fixing_prices', fields=['edate', 'ehour', 'value', 'esiosid'], data=data_list,
                      on_update=on_update)
    logger.info('Cargada info de ESIOS')
    return True

def fixing_spot_price(self,*, mysql, logger, start_date, end_date=None, as_dict=None, on_update=False):
    logger.debug('eSIOS fixing spot prices download')
    list_ids = [600]
    data_list = self.esios_data(logger=logger, start_date=start_date, end_date=end_date, list_ids=list_ids, as_dict=as_dict)
    spot_data_list = []
    for data_list in data_list:
        spot_data_list.append((data_list[0], data_list[1], data_list[2]))
    mysql.insert_data(table='fixing_spot', fields=['edate', 'ehour', 'value'], data=spot_data_list, on_update=on_update)

def forward_esios(self,*, mysql, logger, start_trading_date, end_trading_date, tradate, list_ids=[], as_dict=False, on_update=False):
    logger.debug('eSIOS forecast download')
    data_list = self.esios_data(logger=logger, start_date=start_trading_date, end_date=end_trading_date, list_ids=list_ids, as_dict=as_dict)
    uoload_list = []
    nb_v = len(data_list)
    for idx in range(nb_v):
        row = data_list[idx]
        id_ref = row[3]
        for i in range(idx,min(idx+720,nb_v)):
            aux_row = data_list[i]
            if aux_row[3] == id_ref:
                built_l = [tradate, aux_row[0], aux_row[1], aux_row[2], aux_row[3]]
                uoload_list.append(built_l)
    mysql.insert_data(table='forward_indicators', fields=['tradate', 'edate', 'ehour', 'value', 'esiosid'], data=uoload_list, on_update=on_update)


update_methods = {
    'esios_data': esios_data,
    'kest_data': kest_data,
    'fixing_kest': fixing_kest,
    'fixing_esios': fixing_esios,
    'fixing_spot_price': fixing_spot_price,
    'forward_esios': forward_esios,
}