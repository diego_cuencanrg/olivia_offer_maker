from EsiosWebService import esios_web_functions as web_service
from EsiosWebService import esios_db_manager as db_service
from managers.mysql_manager import MySQL_Manager
import constants as cons
import datetime

raw_date = "2019-11-28"
start_date = datetime.datetime.strptime(raw_date, "%Y-%m-%d")
list_ids = ["793","794"]

# dict_resultados = web_service.esios_data(start_date = start_date, list_ids = list_ids, as_dict= True)

# print(dict_resultados)
dict_values = {
	"user" : cons.USER,
	"password": cons.PASSWORD,
	"host": cons.HOST,
	"database": cons.DATABASE,
	"port": cons.PORT
}
mysql = MySQL_Manager(dict_connection_values=dict_values)
web_service.fixing_esios(mysql=mysql, start_date = start_date, list_ids = list_ids, on_update=True)

