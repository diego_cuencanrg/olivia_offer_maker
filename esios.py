import sys
import csv
import json
import datetime

import xlrd
import requests

import pytz


# sys.setdefaultencoding('utf-8')

# Change floating criteria
def localize_floats(row):
	return [
		str(el).replace('.', ',') if isinstance(el, float) else el
		for el in row
	]


def get_timezone(date):
	timezone = 'Europe/Madrid'
	n_date = pytz.timezone('Europe/Madrid').localize(date.replace(tzinfo=None))
	return n_date.strftime("%Z") + ":00"


# Raw download functions
def esios_data(*, start_date, end_date=None, list_ids=[], as_dict=False, csv_output=None, delimiter_input):
	if not start_date:
		return False
	else:
		if isinstance(start_date, datetime.datetime) or isinstance(start_date, datetime.date):
			query_startDate = start_date
		else:
			query_startDate = datetime.datetime.strptime(start_date, "%Y-%m-%d")

	if not end_date:
		query_endDate = start_date + datetime.timedelta(days=1) + datetime.timedelta(hours=24)
	else:
		if isinstance(end_date, datetime.datetime) or isinstance(end_date, datetime.date):
			query_endDate = end_date
		else:
			query_endDate = datetime.datetime.strptime(end_date, "%Y-%m-%d") + datetime.timedelta(hours=23)

	base_url = "https://api.esios.ree.es/indicators/{}"
	header_Accept = "application/json; application/vnd.esios-api-v1+json"
	header_Content = "application/json"
	header_Host = "api.esios.ree.es"
	header_Authorization = "ebb93350e68b2c5510c53cd1956e60096f54a1ba3f16c5567c092d6f972eec7e"
	query_timeagg = "avg"
	query_timetrunc = "hour"
	query_geoagg = "sum"

	# API e-sios parameters to dictionary (II)
	hHeaders = {
		'Accept': header_Accept,
		'Content-Type': header_Content,
		'Host': header_Host,
		'x-api-key': header_Authorization
	}
	pParams = {
		'start_date': query_startDate,
		'end_date': query_endDate,
		'time_agg': query_timeagg,
		'time_trunc': query_timetrunc,
		'geo_agg': query_geoagg
	}

	# Getting data from e-sios
	v = []
	d = {}

	print(base_url, hHeaders, pParams)
	# tz = get_timezone(query_startDate)
	for row in list_ids:
		# Download Esios
		print('Download ID: ' + str(row))
		r = requests.get(base_url.format(row), headers=hHeaders, params=pParams)
		JSONtext = str(r.content)
		JSONLen = len(JSONtext) - 1
		JSONtext = format_text_esios(JSONtext)
		JSONData = json.loads(JSONtext)
		for x in JSONData["indicator"]["values"]:
			init_fecha = str(x["datetime"])
			qfecha = init_fecha.replace("T", " ")
			qhora = int(init_fecha[11:13]) + 1
			qvalor = x["value"]
			if not as_dict:
				# Export as list
				try:
					pais = x["geo_name"]
				except KeyError:
					v.append((qfecha, qvalor, str(row)))
				else:
					if pais[0:3] == 'Esp':
						v.append((qfecha, qvalor, str(row)))
			else:
				# Export as dict
				try:
					pais = x["geo_name"]
				except KeyError:
					tuple = [str(row), qfecha, qvalor]
					if qfecha not in v:
						v.append(qfecha)
					if qfecha in d.keys():
						d[qfecha].append(tuple)
					else:
						d[qfecha] = []
						d[qfecha].append(tuple)
				else:
					if pais[0:3] == 'Esp':
						tuple = (str(row), qfecha, qvalor)
						if qfecha not in v:
							v.append(qfecha)
						if qfecha in d.keys():
							d[qfecha].append(tuple)
						else:
							d[qfecha] = []
							d[qfecha].append(tuple)

	print(csv_output)
	if csv_output:
		with open(csv_output, "w", newline="") as fid:
			writer = csv.writer(fid, delimiter=delimiter_input)
			for row in v:
				if delimiter_criteria == "ES":
					writer.writerow(localize_floats(row))
				else:
					writer.writerow(row)

	if not as_dict:
		return v
	else:
		return v, d

def format_text_esios(text):
    new_text = text.replace('\\xc3\\xa1','á')
    new_text = new_text.replace('\\xc3\\xa9','é')
    new_text = new_text.replace('\\xc3\\xad','í')
    new_text = new_text.replace('\\xc3\\xb3','ó')
    new_text = new_text.replace('\\xc3\\xb1','ñ')
    new_text = new_text.replace('\\n','')
    text_len = len(new_text) - 1
    new_text = new_text[2:text_len]
    # json.loads(new_text)
    return new_text


start_date = sys.argv[1]
end_date = sys.argv[2]
from datetime import timedelta, date

start_date = date.today() - timedelta(days=1)
end_date = date.today()
ids = [600]
csv_out = True
delimiter_criteria = "ES"

if delimiter_criteria == "ES":
	delimiter_input = ';'
else:
	delimiter_input = ','

esios_data(start_date=start_date, end_date=end_date, list_ids=ids, as_dict=False, csv_output=csv_out,
           delimiter_input=delimiter_input)