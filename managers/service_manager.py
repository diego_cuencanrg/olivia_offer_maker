import time
from functools import partial

from nrg_sf.eNRGPower.runtime_functions import *
from nrg_sf.eNRGPower.constants import *
import nrg_sf.eNRGPower.managers.mysql_manager as mysql_manager
import nrg_sf.eNRGPower.managers.email_manager as email_manager
from nrg_sf.eNRGPower.managers.persistence_manager import *
import nrg_sf.common_utils.tools as tools

class AppServerSvc:
    import nrg_sf.eNRGPower
    _svc_name_ = nrg_sf.eNRGPower.name
    _svc_display_name_ = nrg_sf.eNRGPower.description

    def __init__(self,*, logger, conn_file, email_settings, mode, running_function, decript_key, seconds_delay=None,
                 **kwargs):

        self.slug = 'appservice'
        self.logger = logger
        self.conn_file = conn_file
        self.email_settings = email_settings
        self.decript_key = decript_key
        self.seconds_delay = seconds_delay if seconds_delay else SERVICE_SECONDS_DELAY
        self.kwargs = kwargs

        setattr(self, 'service_mode', partial(running_function, self))
        setattr(self, 'service_mode_name', running_function.__name__)
        setattr(self, 'mode', mode)

        self.set_objects()
        self.logger.debug('Service Initialized')

    def __str__(self):
        return 'Service ´{}´ running; {} seconds delay between operations'.format(self.service_mode_name,self.seconds_delay)
    def set_objects(self):
        self.mysql = mysql_manager.MySQL_Manager(
            json_connection_file=self.conn_file,
            logger=self.logger
        )
        self.email = email_manager.Email_Manager(
            email_settings=self.email_settings,
            logger=self.logger,
            decript_key=self.decript_key
        )
        self.persistence = PersistenceManager(
            logger=self.logger,
        )

    def start_service(self):
        self.logger.info(self)
        try:
            while 1:
                # Checking for operative network
                check_network = tools.check_network()
                check_database = self.mysql.check_connection_to_database()
                check_email = True #self.email.check_email(mode='send')
                checking_list = [check_network, check_database, check_email]
                if all(checking_list):
                    self.logger.info('All systems works properly, service  still running.')
                    self.service_mode(
                        logger=self.logger,
                        mysql=self.mysql,
                        email=self.email,
                        persistence=self.persistence,
                        **self.kwargs
                    )
                    self.logger.debug('Waiting for next step in {} seconds'.format(self.seconds_delay))
                else:
                    if not check_network:
                        self.logger.critical('No internet connection.')
                    if not check_email:
                        self.logger.critical('No email connection.')
                    if not check_database:
                        self.logger.critical('No database connection.')
                    if all([check_network, check_email]) and not check_database:
                        self.email.send_alert(
                            alert_msg='Application has not connection to database, please check.',
                            alert_subject='DATABASE ERROR'
                        )
                    self.logger.critical('Service will not do actions until all systems works properly.')
                if self.seconds_delay > 0:
                    time.sleep(self.seconds_delay)
                else:
                    break
        except Exception as e:
            self.logger.exception(e)


def service(*, logger, conn_file, mode, email_settings, decript_key, **kwargs):
    if mode in modes:
        service = AppServerSvc(
            logger=logger,
            conn_file=conn_file,
            running_function=modes[mode],
            mode=mode,
            email_settings=email_settings,
            decript_key=decript_key,
            **kwargs
        )
        return service
    else:
        logger.critical('{} mode does not exists in application reconized modes, please check.'.format(mode))
        return None
