import re
import os
import csv
import shutil
import datetime
from collections import deque
from pathlib import Path

import ftplib
import fnmatch
import pandas as pd

from nrg_sf.eNRGPower.runtime_functions import *
from nrg_sf.eNRGPower.constants import *
import nrg_sf.common_utils.tools as tools

NETWORK_DRIVE_RE = re.compile(r'^([a-zA-Z]:)')
__all__ = ['Data_Source', 'CSV_Data_Source', 'XLS_Data_Source', 'PersistenceManager']

class PersistenceManager:
    
    def __init__(self, *, logger=None, mysql=None, email=None, ftp_config=None):
        self.slug = 'persistence'
        self.logger = tools.check_logger(logger)
        self.ftp = None
        self._ftp_config = None
        if ftp_config:
            self.check_ftp(ftp_config)
        logger.debug('Persistence Manager Initialized')

    def check_ftp(self, ftp_config):
        ftp_recognized_parameters = [
            'user',
            'password',
            'host',
            'port',
        ]
        ftp_tested_parameters = []
        for parameter in ftp_recognized_parameters:
            if parameter in ftp_config:
                setattr(self, '_ftp_{}'.format(parameter), True)
                ftp_tested_parameters.append(True)
            else:
                setattr(self, '_ftp_{}'.format(parameter), False)
                ftp_tested_parameters.append(False)
        if all(ftp_tested_parameters):
            if tools.check_network():
                ftp = self.connect_to_ftp()
                if ftp:
                    ftp.quit()
                    self.logger.info('FTP correctly configurated')
                    self._ftp_config = ftp_config
                    self.ftp = True
                    return True
            else:
                self.logger.critical('No internet connection.')
        else:
            for idx, flag in enumerate(ftp_tested_parameters):
                self.logger.warning('FTP parameter {} not in configuration'.format(ftp_recognized_parameters[idx]))
        self.ftp = None
        self._ftp_config = None
        return False

    def connect_to_ftp(self):
        ftp_config = self.ftp_config
        if ftp_config:
            if tools.check_network():
                ftp = ftplib.FTP(host=ftp_config['host'])
                ftp.connect(port=ftp_config['port'])
                try:
                    ftp.login(user=ftp_config['user'], passwd=ftp_config['password'])
                    return ftp
                except:
                    return None
                    self.logger.warning('Error with credentials, plase check user and password.')
            else:
                self.logger.critical('No internet connection.')

    def export_bytes_to_ftp(self,*, bytes_text, ftp_filename):
        if self.ftp:
            ftp = self.connect_to_ftp()
            ftp.storlines("STOR " + filename, open(filename, 'rb'))
            ftp.quit()
        else:
            check = self.check_ftp()
            if check:
                ftp = self.connect_to_ftp()
                ftp.storlines("STOR {}".format(ftp_filename), bytes_text)
                ftp.quit()

    def export_file_to_ftp(self,*, local_filename, ftp_filename):
        self.export_bytes_to_ftp(
            bytes_text=open(local_filename, 'rb'),
            ftp_filename=ftp_filename
        )

    def export_string_to_ftp(self,*, string, ftp_filename):
        self.export_bytes_to_ftp(
            bytes_text=string.encode(),
            ftp_filename=ftp_filename
        )

    @property
    def ftp_config(self):
        if self._ftp_config:
            return self._ftp_config
        else:
            self.logger.warning('No FTP parameters available')
            return None

    def export_dataframe_to_excel(self,*, excel_filename, dataframe, sheet='Sheet1', new_file=False, transpose=False, starting_row=0, starting_column=0, header=False, index=False):
        if excel_filename.endswith('.xlsx'):
            if not new_file:
                excel_file = Path(excel_filename)
                if not excel_file.is_file():
                    self.logger.warning('{} file does not exists.'.format(excel_filename))
                    return False
            writer = pd.ExcelWriter(
                excel_filename,
                engine='xlsxwriter'
            )
        else:
            self.logger.warning('{} is not a valid file, method needs a XLSX file.'.format(excel_filename))
            return False
        if transpose:
            dataframe = dataframe.transpose()
        try:
            dataframe.to_excel(
                writer,
                sheet_name=sheet,
                startrow=starting_row,
                startcol=starting_column,
                header=header,
                index=index
            )
            writer.save()
            return True
        except Exception as e:
            self.logger.exception(e)
            return False

    def export_dataframe_to_csv(self,*, csv_filename, dataframe, delimiter=';', extends=False, transpose=False, header=True, index=True):
        if extends:
            csv_file = Path(csv_filename)
            if csv_file.is_file():
                csv_file = open(csv_filename, 'a')
            else:
                self.logger.warning('{} file does not exists.'.format(excel_filename))
                return False
        else:
            if os.path.exists(csv_filename):
                os.remove(csv_filename)
            csv_file = csv_filename
        if transpose:
            dataframe = dataframe.transpose()
        try:
            dataframe.to_csv(
                path_or_buf=csv_file,
                sep=delimiter,
                header=header,
                index=index
            )
            return True
        except Exception as e:
            self.logger.exception(e)
            return False


class DriveNotFound(Exception):
    pass

class Data_Source(object):
    name = 'dummy_name'
    slug = 'dummy_slug'
    origin = None
    filename = ''
    path = ''
    description = None
    datedelta = 0
    offset_hours = 0
    max_retrieval_attempts = 1
    failure_handling = None
    default_output = None
    default_df = None
    outputs_df_dict = False
    source_type = None
    options = None
    lowercase_index_names = False
    lowercase_column_names = False
    drop_columns = []
    rename_columns = []
    df_slice = None
    dropna = None
    fillna = 0.0
    fillna_columns = None
    dtype = float

    def __init__(self, name=None, slug=None, filename_args=None, index=None, lowercase_index_names=None,
                 lowercase_column_names=None, drop_columns=None, rename_columns=None,
                 df_slice=None, dropna=None, fillna=None, fillna_columns=None, dtype=None, **kwargs):

        self.name = name or getattr(self, 'name')
        self.slug = slug or getattr(self, 'slug')
        self.origin = getattr(self, 'origin')
        self.debug = {}
        self._basedate = date
        self.filename_args = filename_args or {}
        self.description = getattr(self, 'description')
        self.unavailable_drives = set()
        self.unavailable_files = set()
        self.default_output = getattr(self, 'default_output')
        self.default_df = getattr(self, 'default_df')
        self.outputs_df_dict = getattr(self, 'outputs_df_dict')
        self.parsed = False
        self.options = options or getattr(self, 'options') or {}
        self.options = self.options.copy()
        self.index = index or getattr(self, 'index')
        self.lowercase_index_names = getattr(self, 'lowercase_index_names')
        self.lowercase_column_names = getattr(self, 'lowercase_column_names')
        self.drop_columns = drop_columns or getattr(self, 'drop_columns')
        self.rename_columns = rename_columns or getattr(self, 'rename_columns')
        self.df_slice = df_slice or getattr(self, 'df_slice')
        self.dropna = dropna or getattr(self, 'dropna')
        self.fillna = fillna if fillna is not None else getattr(self, 'fillna')
        self.fillna_columns = fillna_columns or getattr(self, 'fillna_columns')
        self.dtype = dtype or getattr(self, 'dtype')
        self.availability_checked = False
        self.check_file_availability()

    @property
    def date(self):
        try:
            return self._date
        except AttributeError:
            return None

    @property
    def filename(self):
        return self._filename

    @property
    def path(self):
        return self._path

    @property
    def output(self):
        try:
            return self._output
        except AttributeError:
            if self.local_file_available:
                self._output = self.parse_file()
                self.parsed = True
                return self._output

    def check_file_availability(self):
        # Check it
        self.availability_checked = True

    def parse_file(self):
        df = self.get_data_frame()
        df = self.process_data_frame(df)
        return df

    def get_data_frame(self):
        if self.source_type == 'csv':
            return pd.read_csv(self.local_filepath, **self.options)
        elif self.source_type == 'xls':
            return pd.read_excel(self.local_filepath, **self.options)
        else:
            return None

    def process_data_frame(self, df):
        if self.df_slice:
            df = df[self.df_slice]
        if self.lowercase_index_names:
            df.index = df.index.str.lower()
        if self.lowercase_column_names:
            df.columns = df.columns.str.lower()
        if self.rename_columns:
            df = df.rename(columns=self.rename_columns)
        if self.drop_columns:
            df = df.drop(self.drop_columns, axis=1)
        if self.dropna:
            df = df.dropna(0, 'all')
        if self.fillna is not None:
            if self.fillna_columns:
                df[self.fillna_columns] = df[self.fillna_columns].fillna(self.fillna)
            else:
                df = df.fillna(self.fillna)
        if self.dtype:
            df = df.astype(self.dtype)
        return df

    def __repr__(self):
        filename = self.filename
        if not self.local_file_available:
            filename = '[NOT FOUND] {}'.format(filename)
        return '<Data Source \'{}\'>'.format(filename)

class CSV_Data_Source(Data_Source):
    source_type = 'csv'

class XLS_Data_Source(Data_Source):
    source_type = 'xls'