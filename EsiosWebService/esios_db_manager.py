__all__ = ['EsiosDatabaseManager']

import importlib
from functools import partial

import pandas as pd
# import nrg_sf.common_utils.tools as tools

class EsiosDatabaseManager:

    def __init__(self, *, logger=None, mysql=None, email=None, download_folder=None):

        self.slug = 'esios_db'
        for att in (mysql, email):
            setattr(self, att.slug, att)
        # self.logger = tools.check_logger(logger)
        # self.logger.debug('Esios Database Manager Initialized')
        self.download_folder = download_folder

        esios_web_functions = importlib.import_module('nrg_sf.eNRGPower.commodities.spainish_power.EsiosWebService.esios_web_functions')
        for method in esios_web_functions.update_methods:
            setattr(self, method, partial(esios_web_functions.update_methods[method], self))


    @property
    def fixing_id_list(self):
        try:
            q='SELECT esiosid FROM fixing_prices GROUP BY esiosid'
            d=[]
            cursor=self.mysql.execute_procedure(query=q, data=d)
            lst_out=[]
            for result in cursor:
                lst_out.append(result[0])
            return lst_out
        except:
            return []


    def fixing_hourly_check(self,*,idx=None, start_date=None, end_date=None, under24=False, iqual24=False):
        q='SELECT esiosid,edate,count(esiosid) as numero FROM fundamentales.fixing_indicators{WHERE}{ESIOSID}{AND1}{DATES} GROUP BY esiosid, edate ORDER BY esiosid, edate;'
        where = ' WHERE ' if any([idx, start_date, end_date]) else ''
        if isinstance(idx, list):
            st=''
            for ix, final in tools.lookahead(idx):
               st+= str(ix) + ', ' if final == True else str(ix)
            esiosid = ' esiosid in({})'.format(st)
        else:
            esiosid = ' esiosid={}'.format(idx) if idx else ''
        if all([start_date, end_date]):
            dates = "(edate BETWEEN '{}' AND '{}')".format(start_date, end_date)
        else:
            dates = "edate>='{}'".format(start_date) if start_date else ''
        and1 = ' AND ' if all([esiosid, dates]) else ''
        query=q.format(WHERE=where, ESIOSID=esiosid, AND1=and1, DATES=dates)
        cursor=self.mysql.execute_procedure(query=query)
        columns=[]
        for f in cursor._result.fields:
            columns.append(f.name)
        array=[]
        for row in cursor:
            line=[]
            for value in row:
                line.append(value)
            array.append(line)
        df = pd.DataFrame(array, columns=columns)
        if all([under24,iqual24]):
            return df
        if under24:
            df = df.loc[df.numero < 24, columns]
        if iqual24:
            df = df.loc[df.numero == 24, columns]
        return df


    def upload_esios_fixing(self, idx=None, start_date=None, end_date=None, on_update=False):
        if isinstance(idx, list):
            pass
        elif any([isinstance(idx, int), isinstance(idx, float)]):
            idx = [int(idx)]
        else:
            return False

        if self.mysql:
            self.fixing_esios(
                mysql=self.mysql,
                logger=self.logger,
                start_date=start_date,
                end_date=end_date,
                list_ids=idx,
                on_update=on_update
            )


    def upload_esios_spot(self, start_date=None, end_date=None, on_update=False):
        if self.mysql:
            self.fixing_spot_price(
                mysql=self.mysql,
                logger=self.logger,
                start_date=start_date,
                end_date=end_date,
                on_update=on_update
            )

    
    def upload_kest_fixing(self, start_date=None, on_update=False):
        if self.mysql:
            self.fixing_kest(
                mysql=self.mysql,
                logger=self.logger,
                start_date=start_date,
                on_update=on_update,
                download_folder=self.download_folder
            )
            
            
    def upload_esios_forward(self, idx=None, start_date=None, end_date=None, tradate = None, on_update=False):
        if isinstance(idx, list):
            pass
        elif any([isinstance(idx, int), isinstance(idx, float)]):
            idx = [int(idx)]
        else:
            return False

        if all([self.mysql, tradate, start_date]):
            self.forward_esios(
                mysql=self.mysql,
                logger=self.logger,
                tradate=tradate,
                start_trading_date=start_date,
                end_trading_date=end_date,
                list_ids=idx,
                on_update=on_update
            )
