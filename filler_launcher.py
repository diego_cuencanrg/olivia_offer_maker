from EsiosWebService import esios_web_functions as web_service
from managers.mysql_manager import MySQL_Manager
import constants as cons
import datetime
from datetime import timedelta, date
import logging.config,logging
import offer_maker, index_maker
from EsiosWebService.data_checker import *
from utils.offer_tools import check_if_index
import datetime
from datetime import date, datetime


logging.config.fileConfig("C:/Users/Administrator/Documents/Comercializadora - Developer/Herramientas/Olivia OfferOmie/logging.cfg")
logger = logging.getLogger("OliviaOfertMaker")


mysql = MySQL_Manager(dict_connection_values= cons.dict_values_DB, logger = logger)

def fill_index(date_to_look):
	# Chequea que hay índices para los últimos 15 días y en caso contrario los calcula
    search = check_if_index(date_to_look, mysql)
    if search != 24:
        index_maker.main_runner(date_inicial=date_to_look, mysql=mysql)
    date_to_look = date_to_look + timedelta(days=1)


days_to_look = [
    datetime.strptime("2020-10-21", "%Y-%m-%d")
]

for day in days_to_look:
    fill_index(day)