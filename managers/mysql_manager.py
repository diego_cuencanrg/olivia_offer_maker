import json
import os

import pandas as pd
import pymysql

from utils import tools
class MySQL_Manager:

    def __init__(self,*, json_connection_file=None, dict_connection_values=None, logger=None):

        self.slug = 'mysql'

        self.recognized_parameters = [
            'user',
            'password',
            'host',
            'port',
            'database'
        ]

        self.logger = tools.check_logger(logger)
        logger.debug('MySQL Manager Initialized')

        if json_connection_file:
            if os.path.exists(json_connection_file):
                with open(json_connection_file) as data_file:
                    json_con_data = json.load(data_file)
                for parameter in self.recognized_parameters:
                    if parameter in json_con_data['conn_values']:
                        setattr(self, parameter, json_con_data['conn_values'][parameter])

        elif dict_connection_values:
            for parameter in self.recognized_parameters:
                if parameter in dict_connection_values:
                    setattr(self,parameter,dict_connection_values[parameter])

        else:
            pass

    @property
    def connection_values(self):
        values = {}
        for parameter in self.recognized_parameters:
            if hasattr(self, parameter):
                values[parameter] = getattr(self,parameter)
        return values

    def __str__(self):
        if 'database' in self.connection_values:
            database = getattr(self,'database')
        else:
            database = 'no database'

        if 'host' in self.connection_values:
            host = getattr(self,'host')
        else:
            host = 'no server'
        return 'MySQL Manager Instance for Database `{}` at Host `{}`'.format(database,host)

    def connect_to_db(self):
        if (hasattr(self, 'user'),
            hasattr(self, 'password'),
            hasattr(self, 'host'),
            hasattr(self, 'database')):

            if hasattr(self, 'port'):
                cnx = pymysql.connect(user = self.user,
                                      password = self.password,
                                      host = self.host,
                                      database = self.database,
                                      port = int(self.port))
            else:
                cnx = pymysql.connect(user = self.user,
                                      password = self.password,
                                      host = self.host,
                                      database = self.database)
            return cnx
        else:
            self.logger.debug('Not enought parameters for connect to db')
            return None

    def execute_procedure(self, *, query, data=None, to_dataframe=False):
        data = data if data else []
        cnx = self.connect_to_db()
        if cnx:
            cursor = cnx.cursor()
            try:
                cursor.execute(query, data)
                cnx.commit()
                if not to_dataframe:
                    return cursor
                else:
                    columns = []
                    for f in cursor._result.fields:
                        columns.append(f.name)
                    array = []
                    for row in cursor:
                        line = []
                        for value in row:
                            line.append(value)
                        array.append(line)
                    # array.pop(2)
                    df = pd.DataFrame(array, columns=columns)
                    return df
            except Exception as e:
                self.logger.error(query)
                self.logger.error(e)
                return False
        else:
            self.logger.debug('Not enought parameters for connect to db.')
            return None

    def insert_data(self,*, table, fields=[], data=[], custom={}, ignore_flag=False, on_update=None):
        cnx = self.connect_to_db()
        if cnx:
            str_fields = ""
            values = ''
            for val,flg,idx in tools.lookahead_enumerate(fields):
                if flg == False:
                    final = ""
                else:
                    final = ", "
                if isinstance(custom, dict):
                    if (idx-1) in custom.keys():
                        str_fields += '`{}`{}'.format(val, final)
                        str_fields += custom[idx-1] + final
                    else:
                        str_fields += '`{}`{}'.format(val, final)
                        values += "%s" + final
                else:
                    str_fields += val + final
                    values += "%s" + final

            if not ignore_flag:
                ignore=''
            else:
                ignore=' IGNORE'

            if isinstance(on_update, bool) and on_update == True:
                fields_to_update = ''
                for field, nxt in tools.lookahead(fields):
                    fields_to_update += '{F} = VALUES({F}), '.format(F=field) if nxt else '{F} = VALUES({F})'.format(F=field)
                update = fields_to_update
                update = ' ON DUPLICATE KEY UPDATE {}'.format(update)
            elif on_update:
                update = ' ON DUPLICATE KEY UPDATE {}'.format(on_update)
            else:
                update = ''

            query = 'INSERT{IGNORE} INTO {TABLE} ({FIELDS}) VALUES ({VALUES}){UPDATE}'.format(
                    TABLE=table,
                    FIELDS=str_fields,
                    VALUES=values,
                    IGNORE=ignore,
                    UPDATE=update
            )
            cursor = cnx.cursor()
            try:
                cursor.executemany(query, data)
                lstid = cursor.lastrowid
                cnx.commit()
                cnx.close()
                return lstid
            except Exception as e:
                print(e)
                self.logger.error(e)
                return False
        else:
            self.logger.debug('Not enought parameters for connect to db')
            return None

    def insert_data2(self, table, fields=[], data=[], ignore_flag = False, on_update = None):
        pass

    def check_connection_to_database(self):
        try:
            cnx = self.connect_to_db()
            return True
        except:
            return False
